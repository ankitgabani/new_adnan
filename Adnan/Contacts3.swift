//
//  Contacts3.swift
//  Adnan
//
//  Created by Chintan Patel on 22/11/19.
//  Copyright © 2019 Chintan Patel. All rights reserved.
//

import UIKit
import AVFoundation

class Contacts3: UIViewController,AVAudioPlayerDelegate {
    
    
    var audioPlayer: AVAudioPlayer?
    
    @IBOutlet var use_text: UITextView!
    @IBOutlet var use_view: UIView!
    
    var Ahud2: ATMHud?
    override func viewDidLoad() {
        super.viewDidLoad()
        //let mp3Url = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("Jarah_splash.mp3").absoluteString)
        var err: Error?
        do {
            //audioPlayer = try AVAudioPlayer(contentsOf: mp3Url)
        } catch let err {
        }
        
        Ahud2 = ATMHud(delegate: self)
        if let view1 = Ahud2?.view {
            view.addSubview(view1)
        }
        // Do any additional setup after loading the view.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func GoBackHome(_ sender : UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name("gotoHome"), object: nil)
    }
    
    @IBAction func share(_ sender : Any) {
        playClick()
        use_text.resignFirstResponder()
        if !(use_text.text == "") {
            let textToShare = "لأطفالكم بهلول الراعي itunes.apple.com/us/app/jrt-aldhhb/id916568426 @kidsappsa"
            let imageToShare = getScreenGrab(use_view)
            
            
            
            let itemsToShare = [textToShare, imageToShare] as [Any]
            //NSArray *itemsToShare = @[textToShare];
            let activityVC = UIActivityViewController(activityItems: itemsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [
                .print,
                .copyToPasteboard,
                .assignToContact,
                .saveToCameraRoll
            ]
            
            
            
            present(activityVC, animated: true)
            
            
            let presentationController = activityVC.popoverPresentationController
            
            presentationController?.sourceView = view
            
            
            
            //        if ([self respondsToSelector:@selector(presentViewController:animated:completion:)]){
            //            [self presentViewController:activityVC animated:YES completion:nil];
            //        } else {
            //            [self presentModalViewController:activityVC animated:YES];
            //        }
        } else {
            alert_Auto("الرجاء إكمال البيانات")
        }
        
    }
    
    func getScreenGrab(_ view: UIView?) -> UIImage? {
        if UIScreen.main.responds(to: #selector(getter: UIScreen.scale)) {
            UIGraphicsBeginImageContextWithOptions(view?.bounds.size ?? CGSize.zero, _: false, _: UIScreen.main.scale)
        } else {
            UIGraphicsBeginImageContext((view?.bounds.size)!)
        }
        
        let context = UIGraphicsGetCurrentContext()
        
        if let context = context {
            view?.layer.render(in: context)
        }
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return img
    }
    
    func alert_Auto(_ err: String?) {
        view.bringSubviewToFront(Ahud2!.view)
        Ahud2?.blockTouches = true
        Ahud2?.setCaption(err)
        Ahud2?.show()
        Ahud2?.hide(after: 1.0)
    }
    
    ////********************************////
    ////********************************////
    ////********************************////
    // MARK: - Effects
    ////********************************////
    ////********************************////
    ////********************************////
    func playIntro() {
        audioPlayer?.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("intro.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "intro", withExtension: "mp3")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: musicFile!)
        } catch let error {
            
        }
        audioPlayer?.delegate = self
        audioPlayer?.play()
    }
    
    func playIntro1() {
        audioPlayer?.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("intro_pryer_tree.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "intro_pryer_tree", withExtension: "mp3")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: musicFile!)
        } catch let error {
            
        }
        audioPlayer?.delegate = self
        audioPlayer?.play()
    }
    
    func playIntro2() {
        audioPlayer?.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("intro_bostan.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "intro_bostan", withExtension: "mp3")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: musicFile!)
        } catch let error {
            
        }
        audioPlayer?.delegate = self
        audioPlayer?.play()
    }
    
    func playback() {
        audioPlayer?.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("back.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "back", withExtension: "mp3")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: musicFile!)
        } catch let error {
            
        }
        audioPlayer?.delegate = self
        audioPlayer?.play()
    }
    
    func playClick() {
        audioPlayer?.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("click.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "click", withExtension: "mp3")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: musicFile!)
        } catch let error {
            
        }
        audioPlayer?.delegate = self
        audioPlayer?.play()
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("stop Anim")
        
    }
    
}
