//
//  ViewController.swift
//  Adnan
//
//  Created by Chintan Patel on 16/11/19.
//  Copyright © 2019 Chintan Patel. All rights reserved.
//

import UIKit
import AVFoundation


class ViewController: UIViewController, AVAudioPlayerDelegate, SDWebImageManagerDelegate {

    
    var audioPlayer: AVAudioPlayer? = nil
    var audioPlayer2: AVAudioPlayer? = nil
    @IBOutlet var img_adv: UIImageView!
    @IBOutlet var view_share: UIView!
    
    @IBOutlet weak var cont_about_1: UIView!
    @IBOutlet weak var cont_about_2: UIView!
    @IBOutlet weak var cont_about_3: UIView!
    
    @IBOutlet var image_adnan_bg: UIImageView!
    @IBOutlet var image_adnan_eyes: UIImageView!
    @IBOutlet var image_adnan_eyes1: UIImageView!
    @IBOutlet var image_adnan_mouth: UIImageView!
    @IBOutlet var image_adnan_mouth1: UIImageView!
    
    @IBOutlet var image_app_logo_bg: UIImageView!
    @IBOutlet var image_app_logo_txt: UIImageView!
    
    @IBOutlet var button_info: UIButton!
    @IBOutlet var button_bn1: UIButton!
    @IBOutlet var button_bn2: UIButton!
    @IBOutlet var button_bn3: UIButton!
    @IBOutlet var button_bn4: UIButton!
    
    @IBOutlet weak var cont_list: UIView!
    @IBOutlet weak var cont_view: UIView!
    @IBOutlet weak var cont_viewR: UIView!
    
    @IBOutlet weak var MainC: UIView!
    
    
    func degreesToRadians(x : Double) -> Double {
        return (.pi * x / 180.0)
    }
    
    let update_conH: updater? = nil
    var data_store: DB? = nil
    var start = false
    let click: AVAudioPlayer? = nil
    let active_sound = true
    
    var installD: installer? = nil
    
    var NS1: Timer? = nil
    var NS2: Timer? = nil
    var NS3: Timer? = nil
    var headgo = 100
    var active_replay = 1
    
    
    var deltaAngle_start: Float = 0.0
    var track_touch = true
    let track_start = true
    
    var amInside = 0
    
    
    var numberOfSections = 4
    var startTransform: CGAffineTransform!
    var cloves: [SMClove]?
    var currentValue = 0
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func goto_V1(_ sender: Any) {
        active_replay = 1
        
        view.isUserInteractionEnabled = false
        NotificationCenter.default.post(name: NSNotification.Name("gotoViewList"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name("view_1"), object: nil)
        perform(#selector(viewActive), with: nil, afterDelay: 0.5)
    }
    
    @IBAction func goto_V2(_ sender: Any) {
        active_replay = 1
        view.isUserInteractionEnabled = false
        NotificationCenter.default.post(name: NSNotification.Name("gotoViewList"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name("view_2"), object: nil)
        perform(#selector(viewActive), with: nil, afterDelay: 0.5)
    }
    
    @IBAction func goto_V3(_ sender: Any) {
        active_replay = 1
        view.isUserInteractionEnabled = false
        NotificationCenter.default.post(name: NSNotification.Name("gotoViewList"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name("view_3"), object: nil)
        perform(#selector(viewActive), with: nil, afterDelay: 0.5)
    }
    
    @IBAction func goto_V4(_ sender: Any) {
        active_replay = 0
        view.isUserInteractionEnabled = false
        NotificationCenter.default.post(name: NSNotification.Name("gotoViewViewR"), object: nil)
        perform(#selector(viewActive), with: nil, afterDelay: 0.5)
    }
    
    @objc func viewActive() {
        view.isUserInteractionEnabled = true
    }
    
    func backPressed(_ sender: Any?) {
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        dismiss(animated: true)
    }
    
    func resetViews() {
        active_replay = 1
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        view.sendSubviewToBack(cont_about_1)
        view.sendSubviewToBack(cont_about_2)
        view.sendSubviewToBack(cont_about_3)
        view.sendSubviewToBack(cont_list)
        view.sendSubviewToBack(cont_view)
        view.sendSubviewToBack(cont_viewR)
        
        cont_about_1.isHidden = true
        cont_about_2.isHidden = true
        cont_about_3.isHidden = true
        cont_list.isHidden = true
        cont_view.isHidden = true
        cont_viewR.isHidden = true
        
    }
    
    @objc func gotoView1() {
        amInside = 1
        active_replay = 1
        view.isUserInteractionEnabled = false
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        playClick()
        resetViews()
        perform(#selector(viewActive), with: nil, afterDelay: 0.5)
    }
    
    @objc func gotoView2() {
        amInside = 0
        active_replay = 1
        view.isUserInteractionEnabled = false
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        playClick()
        resetViews()
        cont_about_2.isHidden = false
        view.bringSubviewToFront(cont_about_2)
        cont_about_2.fade(in: 0.5, delegate: nil)
        perform(#selector(viewActive), with: nil, afterDelay: 0.5)
    }
    
    @objc func gotoView3() {
        amInside = 0
        active_replay = 1
        view.isUserInteractionEnabled = false
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        playClick()
        cont_about_3.isHidden = false
        view.bringSubviewToFront(cont_about_3)
        cont_about_3.fade(in: 0.5, delegate: nil)
        perform(#selector(viewActive), with: nil, afterDelay: 0.5)
    }
    
    @objc func gotoAbout() {
        amInside = 0
        active_replay = 1
        view.isUserInteractionEnabled = false
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        playClick()
        view.bringSubviewToFront(cont_about_1)
        cont_about_1.fade(in: 0.5, delegate: nil)
        perform(#selector(viewActive), with: nil, afterDelay: 0.5)
    }
    
    @IBAction func goto_about(_ sender: Any) {
        amInside = 0
        active_replay = 1
        view.isUserInteractionEnabled = false
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        playClick()
        view.bringSubviewToFront(cont_about_1)
        cont_about_1.fade(in: 0.5, delegate: nil)
        perform(#selector(viewActive), with: nil, afterDelay: 0.5)
    }
    
    @objc func gotoViewList() {
        amInside = 0
        active_replay = 1
        view.isUserInteractionEnabled = false
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        playClick()
        cont_list.isHidden = false
        view.bringSubviewToFront(cont_list)
        cont_list.fade(in: 0.5, delegate: nil)
        perform(#selector(viewActive), with: nil, afterDelay: 0.5)
    }
    
    @objc func gotoViewView() {
        amInside = 0
        active_replay = 0
        view.isUserInteractionEnabled = false
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        playClick()
        cont_view.isHidden = false
        view.bringSubviewToFront(cont_view)
        cont_view.fade(in: 0.5, delegate: nil)
        perform(#selector(viewActive), with: nil, afterDelay: 0.5)
        
        NotificationCenter.default.post(name: NSNotification.Name("setupScroll"), object: nil)
        
    }
    
    @objc func gotoViewViewR() {
        amInside = 0
        active_replay = 0
        view.isUserInteractionEnabled = false
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        playClick()
        cont_viewR.isHidden = false
        view.bringSubviewToFront(cont_viewR)
        cont_viewR.fade(in: 0.5, delegate: nil)
        perform(#selector(viewActive), with: nil, afterDelay: 0.5)
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        data_store = DB.init()
        
        //    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
        //    [[AVAudioSession sharedInstance] setActive: YES error: nil];
        //    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        
        let tracker = GAI.sharedInstance()?.defaultTracker
        tracker?.set(kGAIScreenName, value: "Home Screen")
        tracker?.send(GAIDictionaryBuilder.createScreenView().build() as! [AnyHashable : Any])
        
        
        
        //update_conH = updater.init()
        
        cont_about_1.isHidden = true
        cont_about_2.isHidden = true
        cont_about_3.isHidden = true
        cont_list.isHidden = true
        cont_view.isHidden = true
        view_share.isHidden = true
        cont_viewR.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(gotoView1), name: NSNotification.Name("gotoHome"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(gotoAbout), name: NSNotification.Name("gotoAbout"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(gotoView1), name: NSNotification.Name("gotoView1"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(gotoView2), name: NSNotification.Name("gotoView2"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(gotoView3), name: NSNotification.Name("gotoView3"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(gotoViewViewR), name: NSNotification.Name("gotoViewViewR"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(gotoViewView), name: NSNotification.Name("gotoViewView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(gotoViewList), name: NSNotification.Name("gotoViewList"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(prepDataEnd), name: NSNotification.Name("prepDataEnd"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateToush), name: NSNotification.Name("updateToush"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(do_update_done), name: NSNotification.Name("do_update_done"), object: nil)
        performSelector(onMainThread: #selector(do_update), with: nil, waitUntilDone: false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(do_go_bg), name: NSNotification.Name("do_go_bg"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(do_go_bg_in), name: NSNotification.Name("do_go_bg_in"), object: nil)
        
        //let mp3Url = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("stanley_knife_score_cut_wood_002.mp3").absoluteString)
        let mp3Url = Bundle.main.url(forResource: "stanley_knife_score_cut_wood_002", withExtension: "mp3")
        
        var err: Error?
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: mp3Url!)
        } catch let err {
        }
        
        do {
            audioPlayer2 = try AVAudioPlayer(contentsOf: mp3Url!)
        } catch let err {
        }
        //    [audioPlayer2 setNumberOfLoops:MAXFLOAT];
        audioPlayer2!.delegate = self
        
        
        numberOfSections = 4
        cloves = [SMClove](repeating: SMClove(), count: 4)
        
        if numberOfSections % 2 == 0 {
            
            buildClovesEven()
        } else {
            
            buildClovesOdd()
        }
        active_replay = 1
        startApp()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        amInside = 1
        do_go_bg_in()
        
    }
    
    @objc func do_go_bg_in() {
        print("do_go_bg_in")
        if !start {
            start = true
            perform(#selector(startAnimAdnan), with: nil, afterDelay: 0.1)
            
            
            playIntro()
            NS2 = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(adnan_mouth_Anim), userInfo: nil, repeats: true)
        }
        
        NS1 = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(adnan_Eyes_AnimC), userInfo: nil, repeats: true)
        headgo = 100
        NS3 = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(goHead), userInfo: nil, repeats: true)
    }
    
    @objc func do_go_bg() {
        image_adnan_mouth.isHidden = false
        image_adnan_mouth1.isHidden = true
        
        if NS1?.isValid ?? false {
            NS1?.invalidate()
        }
        NS1 = nil
        if NS2?.isValid ?? false {
            NS2?.invalidate()
        }
        NS2 = nil
        if NS3!.isValid {
            NS3?.invalidate()
        }
        NS3 = nil
    }
    
    ////********************************////
    ////********************************////
    ////********************************////
    
    // MARK: - Effects
    ////********************************////
    ////********************************////
    ////********************************////
    func playIntro() {
        audioPlayer?.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("Splash_mo01.MP3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "Splash_mo01", withExtension: "MP3")
        do {
            try audioPlayer = AVAudioPlayer(contentsOf: musicFile!)
        } catch _ {
            
        }
        audioPlayer?.delegate = self
        audioPlayer?.play()
    }
    
    func play_head() {
        audioPlayer?.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("Splash_choose.MP3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "Splash_choose", withExtension: "MP3")
        do {
            try audioPlayer = AVAudioPlayer(contentsOf: musicFile!)
        } catch _ {
            
        }
        
        audioPlayer?.delegate = self
        audioPlayer?.play()
    }
    
    func playback() {
        audioPlayer?.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("back.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "back", withExtension: "mp3")
        do {
            try audioPlayer = AVAudioPlayer(contentsOf: musicFile!)
        } catch _ {
            
        }
        audioPlayer?.delegate = self
        audioPlayer?.play()
    }
    
    func playClick() {
        audioPlayer?.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("click.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "click", withExtension: "mp3")
        do {
            try audioPlayer = AVAudioPlayer(contentsOf: musicFile!)
        } catch _ {
            
        }
        audioPlayer?.delegate = self
        audioPlayer?.play()
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if player == audioPlayer {
            if NS2?.isValid ?? false {
                NS2?.invalidate()
            }
            NS2 = nil
            image_adnan_mouth.isHidden = false
            image_adnan_mouth1.isHidden = true
            
            print("stop Anim")
        }
        
    }
    
    @objc func do_update() {
        if ck_connect() {
            update_conH!.updater()
        } else {
            show_ADV()
        }
        
        
    }
    
    @objc func do_update_done() {
        print("doneeeeeee")
        show_ADV()
    }
    
    func show_ADV() {
        print("doneeeeeee")
        
        
        data_store!.get_type("advban", where: " and show = 0")

        if data_store!.nodesIDs.count > 0 {
            if ck_connect() {
                let manager = SDWebImageManager.shared()
                manager?.download(with: URL(string: data_store!.nodesImage[0] as? String ?? ""), options: [], progress: { receivedSize, expectedSize in
                    // progression tracking code
                }, completed: { image, error, cacheType, finished in
                    if image != nil {

                        self.view.bringSubviewToFront(self.view_share)
                        self.img_adv.image = image
                        self.view_share.isHidden = false
                        self.data_store!.updateNid(self.data_store!.nodesIDs![0] as? String)
                    }
                })
            }
        }
        
        
        
    }
    
    @IBAction func show_ADV_Link(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
//        let launchUrl = data_store.nodesBody[0] as? String
//        if let url = URL(string: launchUrl ?? "") {
//            UIApplication.shared.openURL(url)
//        }
    }
    
    @IBAction func show_ADV_LinkBack(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        view_share.isHidden = true
    }
    
    func flushCache() {
        SDWebImageManager.shared()?.imageCache.clearMemory()
        SDWebImageManager.shared()?.imageCache.clearDisk()
    }
    
    func ck_connect() -> Bool {
        var connect_st: Bool?
        let curReach = Reachability()
        let netStatus = curReach.currentReachabilityStatus()
        switch netStatus {
        case NotReachable:
            connect_st = false
        case ReachableViaWWAN:
            connect_st = true
        case ReachableViaWiFi:
            connect_st = true
        default:
            break
        }
        return connect_st ?? false
        
    }
    
    func var_get(_ str: String?) -> String? {
        let defaults = UserDefaults.standard
        
        let strT = defaults.string(forKey: str ?? "")
        if strT == nil {
            return "0"
        }
        return strT
        
    }
    
    func var_set(_ str: String?, val: String?) {
        let prefs = UserDefaults.standard
        
        prefs.set(Int(val ?? "") ?? 0, forKey: str ?? "")
        prefs.synchronize()
    }
    
    func var_setS(_ str: String?, val: String?) {
        var prefs = UserDefaults.standard
        
        prefs.set(val, forKey: str ?? "")
        prefs.synchronize()
    }
    
    @objc func startAnimAdnan() {
        image_adnan_bg.fade(in: 0.5, delegate: nil)
        image_adnan_eyes.fade(in: 0.5, delegate: nil)
        image_adnan_mouth.fade(in: 0.5, delegate: nil)
        
        
        perform(#selector(startAnimLogo), with: nil, afterDelay: 3.0)
        perform(#selector(startAnimBns), with: nil, afterDelay: 5.0)
    }
    
    @objc func adnan_Eyes_AnimC() {
        
        perform(#selector(adnan_Eyes_Anim1), with: nil, afterDelay: 0.1)
        perform(#selector(adnan_Eyes_Anim2), with: nil, afterDelay: 0.2)
        perform(#selector(adnan_Eyes_Anim1), with: nil, afterDelay: 0.3)
        perform(#selector(adnan_Eyes_Anim2), with: nil, afterDelay: 0.4)
    }
    
    @objc func adnan_Eyes_Anim1() {
        
        image_adnan_eyes.isHidden = true
        image_adnan_eyes1.isHidden = false
        
    }
    
    @objc func adnan_Eyes_Anim2() {
        image_adnan_eyes.isHidden = false
        image_adnan_eyes1.isHidden = true
        
    }
    
    @objc func adnan_mouth_Anim() {
        
        if !image_adnan_mouth.isHidden {
            image_adnan_mouth.isHidden = true
            image_adnan_mouth1.isHidden = false
        } else {
            image_adnan_mouth.isHidden = false
            image_adnan_mouth1.isHidden = true
        }
        
    }
    
    @objc func startAnimLogo() {
        
        image_app_logo_bg.slideIn(from: kFTAnimationTop, in: image_app_logo_bg.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
   // image_app_logo_bg?.slideIn(from: 0, in: image_app_logo_bg.superview, duration: 0.4, delegate: self, start:Selector(temp()), stop: Selector(temp()))
        image_app_logo_txt.pop(in: 0.6, delegate: nil)
        
    }
    
    @objc func temp(){
        
    }
    
    
    
    @objc func startAnimBns() {
        view.isUserInteractionEnabled = false
        
        MainC.fade(in: 0.6, delegate: nil)
        //    [MainC slideInFrom:1 duration:1  delegate:self.view];
        MainC.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.6, animations: {
            self.MainC.transform = CGAffineTransform(rotationAngle: CGFloat(self.degreesToRadians(x: -90)))
        })
        perform(#selector(startAnimBns2), with: nil, afterDelay: 0.3)
    }
    
    @objc func startAnimBns2() {
        //    [button_bn2 popIn:.6 delegate:nil];
        MainC.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.6, animations: {
            self.MainC.transform = CGAffineTransform(rotationAngle: CGFloat(self.degreesToRadians(x: -180)))
        })
        perform(#selector(startAnimBns3), with: nil, afterDelay: 0.3)
    }
    
    @objc func startAnimBns3() {
        //    [button_bn3 popIn:.6 delegate:nil];
        MainC.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.6, animations: {
            self.MainC.transform = CGAffineTransform(rotationAngle: CGFloat(self.degreesToRadians(x: -270)))
        })
        perform(#selector(startAnimBns4), with: nil, afterDelay: 0.3)
    }
    
    @objc func startAnimBns4() {
        MainC.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.6, animations: {
            self.MainC.transform = CGAffineTransform(rotationAngle: CGFloat(self.degreesToRadians(x: 0)))
        })
        perform(#selector(viewActive), with: nil, afterDelay: 0.5)
        button_info.pop(in: 0.6, delegate: nil)
        
    }
    
    @objc func updateToush() {
        headgo = 100
    }
    
    func updateToush2() {
        active_replay = 0
    }
    
    @objc func goHead() {
        if active_replay == 0 {
            headgo = 100
        }
        //    NSLog(@"head %d",headgo);
        headgo = headgo - 1
        if headgo == 0 {
            headgo = 100
            NS2 = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(adnan_mouth_Anim), userInfo: nil, repeats: true)
            play_head()
        }
    }
    
    func startApp() {
        let prefs = UserDefaults.standard
        
        let st1 = prefs.string(forKey: "dbCopy")
        let st2 = prefs.string(forKey: "fontsCopy")
        let st3 = prefs.string(forKey: "fontUnZip")
        
        
        if (st1 == "done2") && (st2 == "done") && (st3 == "done") {
            //        [self do_go_bg_in];
        } else {
            print("progress start")
            perform(#selector(prepDataStart), with: nil, afterDelay: 0.1)
        }
    }
    
    @objc func prepDataEnd() {
        print("prepDataEnd")
        //    [self do_go_bg_in];
    }
    
    @objc func prepDataStart() {
        print("prepDataStart")
        
        installD = installer.init()
        installD!.startCopy()
    }
    
    func rotate1() {
        
        MainC.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        UIView.animate(withDuration: 1.0, animations: {
            self.MainC.transform = CGAffineTransform(rotationAngle: CGFloat(self.degreesToRadians(x: 0)))
        })
        
    }
    
    func rotate2() {
        
        MainC.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        UIView.animate(withDuration: 1.0, animations: {
            self.MainC.transform = CGAffineTransform(rotationAngle: CGFloat(self.degreesToRadians(x: 90)))
        })
        
    }
    
    func rotate3() {
        
        MainC.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        UIView.animate(withDuration: 1.0, animations: {
            self.MainC.transform = CGAffineTransform(rotationAngle: CGFloat(self.degreesToRadians(x: 180)))
        })
        
    }
    
    func rotate4() {
        
        MainC.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        UIView.animate(withDuration: 1.0, animations: {
            self.MainC.transform = CGAffineTransform(rotationAngle: CGFloat(self.degreesToRadians(x: 270)))
        })
        
    }
    
    ////********************************////
    ////********************************////
    ////********************************////
    // MARK: - Effects
    ////********************************////
    ////********************************////
    ////********************************////
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        
        if track_start && amInside == 1 {
            let touch = event?.allTouches?.first
            let location = touch?.location(in: view)
            let dist = calculateDistance(fromCenter: location!)
            
            
            print("dist \(dist)")
            if dist < 40 || dist > 460 {
                //NSLog(@"ignoring  tap %f", dist);
                track_touch = false
            } else {
                track_touch = true
                deltaAngle_start = calculateAngle(fromCenter: location!)
                //NSLog(@"deltaAngle  %f", deltaAngle_start);
                
                startTransform = MainC.transform
            }
        }
        
    }
    
    override func touchesMoved(_ touch: Set<UITouch>, with event: UIEvent?) {
        
        if track_touch {
            audioPlayer2!.play()
            let touch2 = event?.allTouches?.first
            let location = touch2?.location(in: view)
            let dist = calculateDistance(fromCenter: location!)
            
            
            
            if dist < 40 && dist > 460 {
                print("drag path too close to the center \(dist)")
            }
            
            
            
            let deltaAngle = calculateAngle(fromCenter: location!)
            
            
            let angleDifference = deltaAngle_start - deltaAngle
            
            
            //NSLog(@"angleDifference %f", angleDifference);
            if startTransform != nil{
                MainC.transform = startTransform.rotated(by: CGFloat(-angleDifference))
            }
            
        }
    }
    
    override func touchesEnded(_ touch: Set<UITouch>, with event: UIEvent?) {
        
        if track_touch {
            
            let radians = atan2f(Float(MainC.transform.b), Float(MainC.transform.a))
            var newVal: CGFloat = 0.0
            
            //print(String(format: " SMClove count %i", cloves!.count()))
            
            
            for c in cloves! {
                
                if c.minValue > 0 && c.maxValue < 0 {
                    
                    if c.maxValue > radians || c.minValue < radians {
                        
                        if radians > 0 {
                            
                            newVal = CGFloat(radians - .pi)
                        } else {
                            
                            newVal = CGFloat(.pi + radians)
                        }
                        currentValue = Int(c.value)
                    }
                } else if radians > c.minValue && radians < c.maxValue {
                    
                    newVal = CGFloat(radians - c.midValue)
                    currentValue = Int(c.value)
                }
            }
            
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.2)
            
            let t = MainC.transform.rotated(by: -newVal)
            MainC.transform = t
            
            UIView.commitAnimations()
            
            //NSLog(@"currentValue %i",currentValue);
            //        [self changeImage];
        }
        
        track_touch = false
        
    }
    
    func calculateDistance(fromCenter point: CGPoint) -> Float {
        
        let center = CGPoint(x: MainC.center.x, y: MainC.center.y)
        
        //NSLog(@"center x = %fs  ,  y = %f ",center.x,center.y);
        
        let dx = Float(point.x - center.x)
        let dy = Float(point.y - center.y)
        return sqrt(dx * dx + dy * dy)
        
    }
    
    func calculateAngle(fromCenter point: CGPoint) -> Float {
        
        let dx = Float(point.x - MainC.center.x)
        let dy = Float(point.y - MainC.center.y)
        return atan2(dy, dx)
    }
    
    func buildClovesEven() {
        
        button_bn1.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        button_bn1.transform = CGAffineTransform(rotationAngle: CGFloat(degreesToRadians(x: 180)))
        
        
        button_bn2.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        button_bn2.transform = CGAffineTransform(rotationAngle: CGFloat(degreesToRadians(x: 90)))
        
        
        
        //    [button_bn3.layer setAnchorPoint:CGPointMake(0.5, 0.5)];
        //    [UIView animateWithDuration:1.0 animations:^{
        //        [button_bn3 setTransform:CGAffineTransformMakeRotation(degreesToRadians(180))];
        //    }];
        
        
        button_bn4.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        button_bn4.transform = CGAffineTransform(rotationAngle: CGFloat(degreesToRadians(x: 270)))
        
        
        let fanWidth: CGFloat = (CGFloat(.pi * 2 / Float(numberOfSections)))
        var mid: CGFloat = 0
        
        for i in 0..<numberOfSections {
            
            let clove = SMClove()
            clove.midValue = Float(mid)
            clove.minValue = Float(mid - (fanWidth / 2))
            clove.maxValue = Float(mid + (fanWidth / 2))
            clove.value = Int32(i)
            
            
            if (clove.maxValue-Float(fanWidth)) < -.pi {
                
                mid = .pi
                clove.midValue = Float(mid)
                clove.minValue = fabsf(clove.maxValue)
            }
            
            mid -= fanWidth
            
            
            //NSLog(@"cl is %@", clove);
            
            cloves!.append(clove)
        }
        
    }
    
    func buildClovesOdd() {
        
        let fanWidth: CGFloat = (CGFloat(.pi * 2 / Double(numberOfSections)))
        var mid: CGFloat = 0
        
        for i in 0..<numberOfSections {
            
            let clove = SMClove()
            clove.midValue = Float(mid)
            clove.minValue = Float(mid - (fanWidth / 2))
            clove.maxValue = Float(mid + (fanWidth / 2))
            clove.value = Int32(i)
            
            mid -= fanWidth
            
            if clove.minValue < -.pi {
                
                mid = -mid
                mid -= fanWidth
            }
            
            
            cloves?.append(clove)
            
            //NSLog(@"cl is %@", clove);
        }
        
    }
    
    


}

