//
//  Contacts2.swift
//  Adnan
//
//  Created by Chintan Patel on 22/11/19.
//  Copyright © 2019 Chintan Patel. All rights reserved.
//

import UIKit
import AVFoundation

class Contacts2: UIViewController, AVAudioPlayerDelegate {
    
    var audioPlayer: AVAudioPlayer?
    
    @IBOutlet var txt1: UITextField!
    @IBOutlet var txt2: UITextField!
    @IBOutlet var txt3: UITextField!
    
    var Ahud: ATMHud?
    var update_con: updater?
    
    @IBAction func GoBackHome(_sender : UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name("gotoHome"), object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let mp3Url = Bundle.main.url(forResource: "Jarah_splash", withExtension: "mp3")
        //let mp3Url = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("Jarah_splash.mp3").absoluteString)
        var err: Error?
//        do {
//            //audioPlayer = try AVAudioPlayer(contentsOf: (mp3Url?)!)
//        } catch let err {
//        }
        audioPlayer?.delegate = self
        
        
        
        update_con = updater.init()
        Ahud = ATMHud(delegate: self)
        self.view.addSubview(Ahud!.view)
        
        NotificationCenter.default.addObserver(self, selector: #selector(do_contacts_done(_:)), name: NSNotification.Name("do_contacts_done"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(do_contacts_donec(_:)), name: NSNotification.Name("do_contacts_donec"), object: nil)
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func do_contacts(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        
        txt1.resignFirstResponder()
        txt2.resignFirstResponder()
        txt3.resignFirstResponder()
        
        if !(txt1.text == "") && !(txt2.text == "") && !(txt3.text == "") {
            if ck_connect() {
                // show HUD
                view.bringSubviewToFront(Ahud!.view)
                Ahud?.setCaption("جاري التحديث")
                Ahud?.show()
                
                
                update_con?.contacts(txt1.text, title: txt2.text, msg: txt3.text)
            } else {
                alert_Auto("لا يوجد إتصال بالشبكة")
            }
        } else {
            alert_Auto("الرجاء إكمال البيانات")
        }
        
        
    }
    
    @objc func do_contacts_done(_ newUserLog: Notification?) {
        let log = newUserLog?.object as? String
        // hide HUD
        Ahud?.hide()
        // show THnaks
        switchToTimer()
    }
    
    @objc func do_contacts_donec(_ newUserLog: Notification?) {
        // hide HUD
        Ahud?.hide()
    }
    
    func alert_Auto(_ err: String?) {
        view.bringSubviewToFront(Ahud!.view)
        Ahud?.blockTouches = true
        Ahud?.setCaption(err)
        Ahud?.show()
        Ahud?.hide(after: 1.0)
    }
    
    func ck_connect() -> Bool {
        var connect_st: Bool = false
        let curReach = Reachability()
        let netStatus = curReach.currentReachabilityStatus()
        switch netStatus {
        case NotReachable:
            connect_st = false
        case ReachableViaWWAN:
            connect_st = true
        case ReachableViaWiFi:
            connect_st = true
        default:
            break
        }
        return connect_st
        
    }
    
    func switchToTimer() {
        NotificationCenter.default.post(name: NSNotification.Name("gotoView3"), object: nil)
    }
    
    ////********************************////
    ////********************************////
    ////********************************////
    // MARK: - Effects
    ////********************************////
    ////********************************////
    ////********************************////
    func playIntro() {
        audioPlayer?.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("intro.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "intro", withExtension: "mp3")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: musicFile!)
        } catch let error {
            
        }
        audioPlayer?.delegate = self
        audioPlayer?.play()
    }
    
    func playIntro1() {
        audioPlayer?.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("intro_pryer_tree.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "intro_pryer_tree", withExtension: "mp3")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: musicFile!)
        } catch let error {
            
        }
        audioPlayer?.delegate = self
        audioPlayer?.play()
    }
    
    func playIntro2() {
        audioPlayer?.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("intro_bostan.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "intro_bostan", withExtension: "mp3")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: musicFile!)
        } catch let error {
            
        }
        audioPlayer?.delegate = self
        audioPlayer?.play()
    }
    
    func playback() {
        audioPlayer?.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("back.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "back", withExtension: "mp3")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: musicFile!)
        } catch let error {
            
        }
        audioPlayer?.delegate = self
        audioPlayer?.play()
    }
    
    func playClick() {
        audioPlayer?.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("click.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "click", withExtension: "mp3")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: musicFile!)
        } catch let error {
            
        }
        audioPlayer?.delegate = self
        audioPlayer?.play()
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("stop Anim")
        
    }
    
    
    
}
