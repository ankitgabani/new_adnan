//
//  Contacts1.swift
//  Adnan
//
//  Created by Chintan Patel on 20/11/19.
//  Copyright © 2019 Chintan Patel. All rights reserved.
//

import UIKit
import AVFoundation

class Contacts1: UIViewController, AVAudioPlayerDelegate {

    @IBOutlet weak var bottomSocialHeightCont: NSLayoutConstraint!
    
    @IBOutlet weak var topLogoHeightCont: NSLayoutConstraint!
    
    var audioPlayer: AVAudioPlayer!
    @IBOutlet var bnMore: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                
            case 1334:
                print("iPhone 6/6S/7/8")
                bottomSocialHeightCont.constant = 75
                topLogoHeightCont.constant = 73

            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                
            case 2436:
                print("iPhone X/XS/11 Pro")
                bottomSocialHeightCont.constant = 85
                topLogoHeightCont.constant = 85

            case 2688:
                print("iPhone XS Max/11 Pro Max")
                
            case 1792:
                print("iPhone XR/ 11 ")
                bottomSocialHeightCont.constant = 85
                topLogoHeightCont.constant = 85

            default:
                print("Unknown")
            }
        }
        
        print("Really Screen Height:= \(UIScreen.main.nativeBounds.height)")
        
        if UIDevice().userInterfaceIdiom == .pad {
            switch UIScreen.main.nativeBounds.height {
            case 2048:
                print("iPad Pro (9.7-inch), iPad Air 2, iPad Mini 4")
                bottomSocialHeightCont.constant = 171
                topLogoHeightCont.constant = 165
            case 2732:
                print("Pad Pro (12.9-inch), iPad Pro 12.9-inch (2nd generation)")
                bottomSocialHeightCont.constant = 210
                topLogoHeightCont.constant = 220

                
            case 1668:
                print("iPad Pro 10.5-inch")
                bottomSocialHeightCont.constant = 176
                topLogoHeightCont.constant = 175

                
            case 2388:
                print("iPad Pro 11-inch")
                bottomSocialHeightCont.constant = 171
                topLogoHeightCont.constant = 175

                
            default:
                print("Unknown")
            }
        }
        
//        let mp3Url = Bundle.main.url(forResource: "Jarah_splash", withExtension: "mp3")
//        var err: Error?
//        do {
//            audioPlayer = try AVAudioPlayer(contentsOf: mp3Url!)
//        } catch let err {
//        }
//        audioPlayer.delegate = self

        // Do any additional setup after loading the view.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func goBackHome(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name("gotoHome"), object: nil)
    }
    
    @IBAction func goView3(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name("gotoView2"), object: nil)
    }
    
    @IBAction func topButtonLinks(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        
        let theButton = sender as? UIButton
        
        var launchUrl = ""
        if theButton?.tag == 1 {
            launchUrl = "http://www.sf.org.sa"
        }
        if theButton?.tag == 2 {
            launchUrl = "http://tag.sa"
        }
        if theButton?.tag == 3 {
            launchUrl = "http://www.youtube.com/user/KidsAppSA"
        }
        if theButton?.tag == 4 {
            launchUrl = "https://plus.google.com/+KidsappSaCenter"
        }
        if theButton?.tag == 5 {
            launchUrl = "http://instagram.com/kidsappsa"
        }
        if theButton?.tag == 6 {
            launchUrl = "http://www.linkedin.com/company/5104985"
        }
        if theButton?.tag == 7 {
            launchUrl = "https://twitter.com/kidsappsa"
        }
        if theButton?.tag == 8 {
            launchUrl = "https://www.facebook.com/kidsappsa"
        }
        if theButton?.tag == 9 {
            launchUrl = "http://kidsapp.sa"
        }
        if let url = URL(string: launchUrl) {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func btnFacebook(_ sender: Any) {
        
        let launchUrl = "https://www.facebook.com/kidsappsa"
        
        if let url = URL(string: launchUrl) {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    @IBAction func btnTwitter(_ sender: Any) {
        let launchUrl = "https://twitter.com/kidsappsa"
        
        if let url = URL(string: launchUrl) {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    @IBAction func btnIn(_ sender: Any) {
        let launchUrl = "http://www.linkedin.com/company/5104985"
        
        if let url = URL(string: launchUrl) {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    @IBAction func btnInstagram(_ sender: Any) {
        let launchUrl = "http://instagram.com/kidsappsa"
        
        if let url = URL(string: launchUrl) {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    @IBAction func btnGoogle(_ sender: Any) {
        let launchUrl = "https://plus.google.com/+KidsappSaCenter"
        
        if let url = URL(string: launchUrl) {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    @IBAction func btnYoutube(_ sender: Any) {
        let launchUrl = "http://www.youtube.com/user/KidsAppSA"
        
        if let url = URL(string: launchUrl) {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func btnOther(_ sender: Any) {
    }
    
    @IBAction func btnKidsAppCompany(_ sender: Any) {
        let launchUrl = "http://kidsapp.sa"
        
        if let url = URL(string: launchUrl) {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    
    @IBAction func btnKidsAppSa(_ sender: Any) {
    }
    
    
    @IBAction func moreApps(_ sender: Any) {
        
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        
        
        let appsViewController = DAAppsViewController.init()
        appsViewController.loadApps(withArtistId: 872947888, completionBlock: nil)
        
        
        let navController = UINavigationController(rootViewController: appsViewController)
        
        let backButton = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(backPressed(_:)))
        
        
        appsViewController.navigationItem.leftBarButtonItem = backButton
        
        
        navController.modalPresentationStyle = .formSheet
        
        if responds(to: #selector(UIViewController.present(_:animated:completion:))) {
            present(navController, animated: true)
        } else {
            
        }
        
    }
    
    @objc func backPressed(_ sender: Any?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func share(_ sender: Any) {
        playClick()
        
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        
        let textToShare = "دع أطفالك يحفظون القرآن الكريم بسهولة مع عدنان معلم القرآن 2 @AdnanQuran  ~ الإصدار الثاني ~ https://itunes.apple.com/us/app/adnan2/id1016304629?ls=1&mt=8     من إنتاج @KidsApp"
        let imageToShare = UIImage(named: "ipad_splash_txt.png")
        
        
        //    NSArray *itemsToShare = @[textToShare, imageToShare];
        let itemsToShare = [textToShare]
        let activityVC = UIActivityViewController(activityItems: itemsToShare, applicationActivities: nil)
        activityVC.excludedActivityTypes = [
            .print,
            .copyToPasteboard,
            .assignToContact,
            .saveToCameraRoll
        ]
        
        let presentationController = activityVC.popoverPresentationController
        
        presentationController?.sourceView = view
        
        if responds(to: #selector(UIViewController.present(_:animated:completion:))) {
            present(activityVC, animated: true)
        } else {
            //presentModalViewController(activityVC, animated: true)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    ////********************************////
    ////********************************////
    ////********************************////
    
    // MARK: - Effects
    ////********************************////
    ////********************************////
    ////********************************////
    func playIntro() {
        audioPlayer.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("intro.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "intro", withExtension: "mp3")
        var err: Error?
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: musicFile!)
        } catch let err {
        }
        audioPlayer.delegate = self
        audioPlayer.play()
    }
    
    func playIntro1() {
        audioPlayer.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("intro_pryer_tree.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "intro_pryer_tree", withExtension: "mp3")
        var err: Error?
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: musicFile!)
        } catch let err {
        }
        audioPlayer.delegate = self
        audioPlayer.play()
    }
    
    func playIntro2() {
        audioPlayer.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("intro_bostan.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "intro_bostan", withExtension: "mp3")
        var err: Error?
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: musicFile!)
        } catch let err {
        }
        audioPlayer.delegate = self
        audioPlayer.play()
    }
    
    func playback() {
        audioPlayer.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("back.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "back", withExtension: "mp3")
        var err: Error?
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: musicFile!)
        } catch let err {
        }
        audioPlayer.delegate = self
        audioPlayer.play()
    }
    
    func playClick() {
        audioPlayer.stop()
        
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("click.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "click", withExtension: "mp3")
        var err: Error?
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: musicFile!)
        } catch let err {
        }
        audioPlayer.delegate = self
        audioPlayer.play()
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("stop Anim")
        
    }

}
