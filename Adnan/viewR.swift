//
//  viewR.swift
//  Adnan
//
//  Created by Chintan Patel on 22/11/19.
//  Copyright © 2019 Chintan Patel. All rights reserved.
//

import UIKit
import AVFoundation

class viewR: UIViewController, AVAudioPlayerDelegate, AVAudioRecorderDelegate, UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource{
    
    var player: AVAudioPlayer? = nil
    var click: AVAudioPlayer? = nil
    
    
    
    @IBOutlet weak var img_eff: UIImageView!
    @IBOutlet weak var bn_record: UIButton!
    @IBOutlet weak var bn_stop: UIButton!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var txt: UILabel!
    
    
    var data_storeVR: DB? = DB.init()
    var counter_min = 0
    var counter_sec = 0
    var NS1C: Timer? = nil
    var recorder: AVAudioRecorder? = nil
    var tempRecFile: URL? = nil
    
    var record = 0
    
    var eff_x: Int = 0
    var eff_y: Int = 0
    var eff_w: Int = 0
    var eff_h: Int = 0
    let eff_sp = 20
    
    var selected_delete = 0
        
    var alertView1: UIAlertView? = nil
    var alertView2: UIAlertView? = nil
    var alertView3: UIAlertView? = nil
    
    var audioSession: AVAudioSession? = nil
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func goBackHome(_ sender: Any) {
        player?.stop()
        reloadAll()
        NotificationCenter.default.post(name: NSNotification.Name("gotoHome"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
    }
    
    @IBAction func goRecord(_ sender: Any) {
        player?.stop()
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        start_record()
    }
    
    @IBAction func goStop(_ sender: Any) {
        player?.stop()
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        stop_record()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layoutIfNeeded()
        eff_x = Int(img_eff.frame.origin.x)
        eff_y = Int(img_eff.frame.origin.y)
        eff_w = Int(img_eff.frame.size.width)
        eff_h = Int(img_eff.frame.size.height)
        
        txt.text = ""
        
        
        //data_storeVR = DB.init()
        
        //let mp3Url = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("Splash_mo01.MP3").absoluteString)
        let mp3Url = Bundle.main.url(forResource: "Splash_mo01", withExtension: "MP3")
        var err: Error?
        
        do {
            player = try AVAudioPlayer(contentsOf: mp3Url!)
        } catch let err {
        }
        //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupScroll)  name:@"setupScroll" object:nil];
        //
        //
        do {
            click = try AVAudioPlayer(contentsOf: mp3Url!)
        } catch let err {
        }
        
        audioSession = AVAudioSession.sharedInstance()
        session_start()
        
        let error: Error? = nil
        
        if error != nil {
            print("error: \(error?.localizedDescription ?? "")")
        } else {
            recorder?.prepareToRecord()
        }
        
    }
    
    func session_start() {
        
        
        do{
            try audioSession?.setActive(true, options: [])
        }catch let err{
            print(err)
        }
        do{
            try audioSession?.setCategory(AVAudioSession.Category.playAndRecord)
        }catch let err{
            print(err)
        }
//
//
//
//        do {
//            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
//            //try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord, withOptions: AVAudioSessionCategoryOptionDefaultToSpeaker)
//        } catch {
//        }
    }
    
    func session_stop() {
        do{
            try audioSession?.setActive(true, options: [])
        }catch let err{
            print(err)
        }
        do{
            try audioSession?.setCategory(AVAudioSession.Category.ambient)
        }catch let err{
            print(err)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        eff_x = Int(img_eff.frame.origin.x)
        eff_y = Int(img_eff.frame.origin.y)
        eff_w = Int(img_eff.frame.size.width)
        eff_h = Int(img_eff.frame.size.height)
        reloadAll()
    }
    
    func reloadAll() {
        session_stop()
        
        record = 0
        reloadAll2()
        
        data_storeVR!.get_type("media", where: "")
        table.reloadData()
    }
    
    func reloadAll2() {
        record = 0
        if NS1C?.isValid ?? false {
            NS1C?.invalidate()
        }
        NS1C = nil
        counter_min = 0
        counter_sec = 0
        updateLabel()
        
        
        bn_record.isHidden = false
        bn_stop.isHidden = true
        img_eff.layer.removeAllAnimations()
        img_eff.frame = CGRect(x: eff_x, y: eff_y, width: eff_w, height: eff_h)
        img_eff.alpha = 1
    }
    
    func makeViewRunEff1() {
        img_eff.alpha = 0.3
        UIView.beginAnimations("fade in", context: nil)
        UIView.setAnimationDuration(1.9)
        UIView.setAnimationRepeatCount(MAXFLOAT)
        UIView.setAnimationRepeatAutoreverses(true)
        img_eff.alpha = 1
        img_eff.frame = CGRect(x: eff_x + eff_sp, y: eff_y + eff_sp, width: eff_w - eff_sp - eff_sp, height: eff_h - eff_sp - eff_sp)
        UIView.commitAnimations()
        
        
        
    }
    
    ////********************************////
    ////********************************////
    ////********************************////
    // MARK: - Table
    ////********************************////
    ////********************************////
    ////********************************////
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? list_cell
        cell?.frame = CGRect(x: 0, y: 0, width: self.table.frame.size.width, height: 40)
        
        if indexPath.row % 2 != 0 {
            cell?.backgroundColor = UIColor(red: 0.996, green: 0.925, blue: 0.647, alpha: 1.00)
        } else {
            cell?.backgroundColor = UIColor(red: 1.000, green: 0.937, blue: 0.835, alpha: 1.00)
        }
        cell?.lb_txt.text = data_storeVR!.nodesTitle[indexPath.row] as! String
        cell?.lb_count.text = "\(indexPath.row + 1)"
        cell?.lb_time.text = data_storeVR!.nodesTeaser[indexPath.row] as! String
        
        return cell!
    }
    
    //---set the number of rows in the table view---
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if data_storeVR!.nodesIDs == nil {
            return 0
        }
        return data_storeVR!.nodesIDs.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        reloadAll2()
        bn_stop.isHidden = false
        bn_record.isHidden = true
        
        print(data_storeVR!.nodesBody[indexPath.row])
        
        
        
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        var docsDir = dirPaths[0]
        //let soundFilePath = URL(fileURLWithPath: docsDir).appendingPathComponent(data_storeVR!.nodesBody[indexPath.row] as? String ?? "").absoluteString
        docsDir = docsDir + "/"
        var soundFilePath = docsDir.appending(data_storeVR!.nodesBody[indexPath.row] as? String ?? "")
        //let mp3Url = URL(string: soundFilePath)
        let mp3Url = URL(fileURLWithPath: soundFilePath)
        var err: Error?
        do {
            player = try AVAudioPlayer(contentsOf: mp3Url)
        } catch let err {
            print(err)
        }
        player?.delegate = self
        player?.play()
        
        NS1C = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        makeViewRunEff1()
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .default, title: "حذف", handler: { action, indexPath in
            // Delete something here
            
            
            self.selected_delete = indexPath.row
            
            self.alertView1 = UIAlertView(title: "تأكيد", message: "هل أنت متأكد", delegate: self, cancelButtonTitle: "إلغاء", otherButtonTitles: "موافق")
            self.alertView1!.show()
            
            
        })
        delete.backgroundColor = UIColor.red
        
        let more = UITableViewRowAction(style: .default, title: "تعديل", handler: { action, indexPath in
            //Just as an example :
            
            self.selected_delete = indexPath.row
            self.alertView3 = UIAlertView(title: "تعديل العنوان", message: "الرجاء إدخال عنوان جديد:", delegate: self, cancelButtonTitle: "حفظ", otherButtonTitles: "إلغاء")
            self.alertView3!.alertViewStyle = .plainTextInput
            
            let textField = self.alertView3!.textField(at: 0)
            textField?.text = self.data_storeVR!.nodesTitle[indexPath.row] as? String
            
            
            
            self.alertView3!.show()
            
        })
        more.backgroundColor = UIColor(red: 1.000, green: 0.792, blue: 0.180, alpha: 1.00)
        
        return [delete, more] //array with all the buttons you want. 1,2,3, etc...
    }
    
    func start_record() {
        session_start()
        
        record = 1
        NS1C = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        makeViewRunEff1()
        updateLabel()
        bn_stop.isHidden = false
        bn_record.isHidden = true
        
        
        
        
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let docsDir = dirPaths[0]
        let soundFilePath = URL(fileURLWithPath: docsDir).appendingPathComponent("tmpSound\(data_storeVR!.nid_max2New() ?? "").caf").absoluteString
        
        tempRecFile = URL(string: soundFilePath)
        
        let recSettings = [
            AVEncoderAudioQualityKey : NSNumber(value: AVAudioQuality.min.rawValue),
            AVEncoderBitRateKey : NSNumber(value: 16),
            AVNumberOfChannelsKey : NSNumber(value: 2),
            AVSampleRateKey : NSNumber(value: 11100.0)
        ]
        
        do {
            if let recSettings = recSettings as? [String : Any] {
                recorder = try AVAudioRecorder(url: tempRecFile!, settings: recSettings)
            }
        } catch {
        }
        recorder!.delegate = self
        recorder!.prepareToRecord()
        recorder!.record()
    }
    
    func stop_record() {
        session_stop()
        if record == 0 {
            player!.stop()
            reloadAll2()
        } else {
            recorder!.stop()
            record = 0
            if counter_sec > 0 || counter_min > 0 {
                
                ////Alert And Save
                
                
                alertView2 = UIAlertView(title: "العنوان", message: "الرجاء إدخال العنوان:", delegate: self, cancelButtonTitle: "حفظ", otherButtonTitles: "إلغاء")
                alertView2!.alertViewStyle = .plainTextInput
                
                
                alertView2!.show()
                
                if NS1C?.isValid ?? false {
                    NS1C?.invalidate()
                }
                img_eff.layer.removeAllAnimations()
                img_eff.alpha = 1
            } else {
                removeImage("tmpSound\(data_storeVR!.nid_max2New() ?? "").caf")
                
                reloadAll()
            }
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if alertView == alertView3 {
            if buttonIndex == 0 {
                let passwordTextField = alertView.textField(at: 0)
                
                data_storeVR!.updateTitle(data_storeVR!.nodesIDs![selected_delete] as! String, title: passwordTextField?.text)
                reloadAll()
            } else {
                reloadAll()
            }
        }
        
        if alertView == alertView2 {
            if buttonIndex == 0 {
                print("tmpSound\(data_storeVR!.nid_max2New() ?? "").caf")
                let passwordTextField = alertView.textField(at: 0)
                data_storeVR!.insert_data(data_storeVR!.nid_max2New(), title: passwordTextField?.text, teaser: txt.text, body: "tmpSound\(data_storeVR!.nid_max2New() ?? "").caf", type: "media", term: "NO", image: "NO", video: "NO", sound: "NO", pdf: "NO", date: "0", change: "0")
                reloadAll()
            } else {
                removeImage("tmpSound\(data_storeVR!.nid_max2New() ?? "").caf")
                reloadAll()
            }
        }
        
        
        if alertView == alertView1 {
            
            if buttonIndex == 0 {
                reloadAll()
            } else if buttonIndex == 1 {
                print("del")
                data_storeVR!.updateNid_sound_remove(data_storeVR!.nodesIDs![selected_delete] as! String)
                removeImage(data_storeVR!.nodesBody![selected_delete] as! String)
                reloadAll()
            }
        }
    }
    
    @objc func updateTime() {
        counter_sec += 1
        
        if counter_sec == 60 {
            counter_min += 1
            counter_sec = 0
        }
        updateLabel()
    }
    
    func updateLabel() {
        txt.text = String(format: "%02d:%02d", counter_min, counter_sec)
    }
    
    func playClick() {
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("click.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "click", withExtension: "mp3")
        do {
            click = try AVAudioPlayer(contentsOf: musicFile!)
        }catch let err {
            
        }
        //click.init(contentsOf: musicFile, error: nil)
        click?.play()
    }
    
    func audioPlayerDidFinishPlaying(_ playerM: AVAudioPlayer, successfully flag: Bool) {
        if playerM == player {
            reloadAll2()
        }
        
        
    }
    
    func removeImage(_ fileName: String?) {
        print("delete file -:\(fileName ?? "") ")
        let fileManager = FileManager.default
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        
        let filePath = URL(fileURLWithPath: documentsPath).appendingPathComponent(fileName ?? "").absoluteString
        var error: Error?
        var success = false
        do {
            try fileManager.removeItem(atPath: filePath)
            success = true
        } catch {
        }
        if success {
            //        UIAlertView *removeSuccessFulAlert=[[UIAlertView alloc]initWithTitle:@"Congratulation:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            //        [removeSuccessFulAlert show];
        } else {
            print("Could not delete file -:\(error?.localizedDescription ?? "") ")
        }
    }

    
}

