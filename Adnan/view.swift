//
//  view.swift
//  Adnan
//
//  Created by Chintan Patel on 22/11/19.
//  Copyright © 2019 Chintan Patel. All rights reserved.
//

import UIKit
import AVFoundation

class view: UIViewController, DMLazyScrollViewDelegate, UIWebViewDelegate, AVAudioPlayerDelegate {
    
    
    @IBOutlet weak var imgGreenFrame: UIImageView!
    @IBOutlet weak var viewTextWork: UIView!
    
    @IBOutlet weak var imgBottom: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnNextL: UIButton!
    @IBOutlet weak var btnPreL: UIButton!
    
    
    @IBOutlet weak var imgTopCont: NSLayoutConstraint!
    @IBOutlet weak var imgRightCon: NSLayoutConstraint!
    @IBOutlet weak var imgLeftCont: NSLayoutConstraint!
    
    @IBOutlet weak var imtTabTopCont: NSLayoutConstraint!
    
    @IBOutlet weak var viewBorromCont: NSLayoutConstraint!
    
    @IBOutlet weak var viewLeftCont: NSLayoutConstraint!
    
    @IBOutlet weak var viewTopCont: NSLayoutConstraint!
    
    @IBOutlet weak var viewRightCont: NSLayoutConstraint!
    
    @IBOutlet weak var imgChange: UIImageView!
    var player: AVAudioPlayer? = nil
    var click: AVAudioPlayer? = nil
    
    let bgTaskId: UIBackgroundTaskIdentifier? = nil
    @IBOutlet weak var viewPage: UIWebView!
    
    
    @IBOutlet weak var bn_play: UIButton!
    @IBOutlet weak var bn_stop: UIButton!
    
    @IBOutlet var image_soora_bgA: UIImageView!
    
    @IBOutlet var button_done: UIButton!
    @IBOutlet var button_udone: UIButton!
    @IBOutlet var button_repeat: UIButton!
    
    @IBOutlet var button_repeat_1: UIButton!
    @IBOutlet var button_repeat_2: UIButton!
    @IBOutlet var button_repeat_3: UIButton!
    
    var data_storeV: DB? = nil
    var sorra = 0
    var sorra_pageNum = 0
    var sorra_pageNum_end = 0
    var pagesNum: Int = 0
    var current_page = 0
    var current_ayah = 0
    var ayah_count = 0
    
    var isRepeatFinished: Bool = false
    var isRealRepeatFinished3: Bool = false
    var isRealRepeatFinished2: Bool = false
    var isRealRepeatFinished1: Bool = false
    
    var isFirstTime: Bool = false
    var repeatt = 0
    var repeat_seq = 0
    
    func LastAnimation() {
        
        UIView.animate(withDuration: 1, animations: {
            
        }, completion: {
            (value: Bool) in
            
            self.imageFirstStartAnimLogo()
            
        })
        
    }
    
    @objc func imageFirstStartAnimLogo() {
        
        image_soora_bgA.isHidden = false
        
        image_soora_bgA.alpha = 0.0
        UIView.animate(withDuration: 2, animations: {
            self.image_soora_bgA.alpha = 1.0
        }, completion: {
            (value: Bool) in
            
            self.startAnimLogo()
            
            self.perform(#selector(self.startAnimFrame), with: nil, afterDelay: 1.0)
            
        })
        
    }
    
    @objc func startAnimFrame() {
        
        imgGreenFrame.isHidden = false
        viewTextWork.isHidden = false
        
        imgGreenFrame.alpha = 0.0
        viewTextWork.alpha = 0.0
        UIView.animate(withDuration: 1, animations: {
            self.viewTextWork.alpha = 1.0
            self.imgGreenFrame.alpha = 1.0
        }, completion: {
            (value: Bool) in
            self.isFirstTime = false
        })
        
    }
    
    
    @objc func startAnimLogo() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.bn_play.isHidden = false
            self.bn_stop.isHidden = true
            
            let prefs = UserDefaults.standard
            
            let myString = prefs.string(forKey: "soorah-\(self.sorra)")
            if (myString == "done") {
                self.button_done.isHidden = true
                self.button_udone.isHidden = false
            } else {
                self.button_done.isHidden = false
                self.button_udone.isHidden = true
            }
        }
        
        btnPreL.isHidden = false
        imgBottom.isHidden = false
        btnBack.isHidden = false
        btnNextL.isHidden = false
        button_repeat.isHidden = false
        
        btnPreL.slideIn(from: kFTAnimationBottom, in: btnPreL.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
        imgBottom.slideIn(from: kFTAnimationBottom, in: imgBottom.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
        btnBack.slideIn(from: kFTAnimationBottom, in: btnBack.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
        btnNextL.slideIn(from: kFTAnimationBottom, in: btnNextL.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
        bn_play.slideIn(from: kFTAnimationBottom, in: bn_play.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
        bn_stop.slideIn(from: kFTAnimationBottom, in: bn_stop.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
        button_udone.slideIn(from: kFTAnimationBottom, in: button_udone.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
        button_done.slideIn(from: kFTAnimationBottom, in: button_done.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
        button_repeat.slideIn(from: kFTAnimationBottom, in: button_repeat.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
        
    }
    
    @IBAction func goPlay(_ sender: Any) {
        playClick()
        if current_ayah == ayah_count && !player!.isPlaying {
            current_ayah = 0
        }
        playerStart()
    }
    
    @IBAction func goNext(_ sender: Any) {
        playClick()
        playerStartNext()
    }
    
    @IBAction func goPrev(_ sender: Any) {
        playClick()
        playerStartPrev()
    }
    
    @IBAction func goStop(_ sender: Any) {
        playClick()
        rest()
        player?.stop()
    }
    
    @IBAction func goBackHome(_ sender: Any) {
        rest()
        player?.stop()
        NotificationCenter.default.post(name: NSNotification.Name("gotoViewList"), object: nil)
    }
    
    @IBAction func sooarRepeat(_ sender: Any) {
        playClick()
        let theButton = sender as? UIButton
        
        
        theButton?.isUserInteractionEnabled = false
        perform(#selector(reActive(_:)), with: theButton, afterDelay: 1.0)
        
        if theButton?.tag == 0 {
            perform(#selector(sooarRepeat_Anim_1), with: nil, afterDelay: 0.1)
            perform(#selector(sooarRepeat_Anim_2), with: nil, afterDelay: 0.2)
            perform(#selector(sooarRepeat_Anim_3), with: nil, afterDelay: 0.3)
        } else if theButton?.tag == 1 {
            repeatt = 1
            isRealRepeatFinished1 = true
            button_repeat.setImage(UIImage(named: "ic_Repet1.png"), for: .normal)
            button_repeat_1.fadeOut(0.3, delegate: nil)
            button_repeat_2.fadeOut(0.3, delegate: nil)
            button_repeat_3.fadeOut(0.3, delegate: nil)
        } else if theButton?.tag == 2 {
            repeatt = 2
            isRealRepeatFinished2 = true
            button_repeat.setImage(UIImage(named: "ic_Repet2.png"), for: .normal)
            button_repeat_1.fadeOut(0.3, delegate: nil)
            button_repeat_2.fadeOut(0.3, delegate: nil)
            button_repeat_3.fadeOut(0.3, delegate: nil)
        } else if theButton?.tag == 3 {
            repeatt = 3
            isRealRepeatFinished3 = true
            button_repeat.setImage(UIImage(named: "ic_Repet3.png"), for: .normal)
            button_repeat_1.fadeOut(0.3, delegate: nil)
            button_repeat_2.fadeOut(0.3, delegate: nil)
            button_repeat_3.fadeOut(0.3, delegate: nil)
        }
        
        
    }
    
    @objc func reActive(_ bn: UIButton?) {
        bn?.isUserInteractionEnabled = true
    }
    
    @objc func sooarRepeat_Anim_1() {
        //[self PlayClick];
        
        button_repeat_1!.slideIn(from: kFTAnimationBottom, in: button_repeat_1.superview, duration: 0.3, delegate: nil, start:Selector("temp") , stop: Selector("temp"))
        //button_repeat_1!.slideIn(from: 2, inView: button_repeat_1.superview, duration: 0.3, delegate: nil, startSelector: nil, stopSelector: nil)
        button_repeat_1.isUserInteractionEnabled = false
        perform(#selector(reActive(_:)), with: button_repeat_1, afterDelay: 0.5)
    }
    
    @objc func temp(){
        
    }
    
    @objc func sooarRepeat_Anim_2() {
        //[self PlayClick];
        
        button_repeat_2!.slideIn(from: kFTAnimationBottom, in: button_repeat_2.superview, duration: 0.3, delegate: nil, start:Selector("temp") , stop: Selector("temp"))
        // button_repeat_2!.slideIn(from: 2, inView: button_repeat_2.superview, duration: 0.3, delegate: nil, startSelector: nil, stopSelector: nil)
        button_repeat_2.isUserInteractionEnabled = false
        perform(#selector(reActive(_:)), with: button_repeat_2, afterDelay: 0.5)
    }
    
    @objc func sooarRepeat_Anim_3() {
        //[self PlayClick];
        
        button_repeat_3!.slideIn(from: kFTAnimationBottom, in: button_repeat_3.superview, duration: 0.3, delegate: nil, start:Selector("temp") , stop: Selector("temp"))
        // button_repeat_3!.slideIn(from: 2, inView: button_repeat_3.superview, duration: 0.3, delegate: nil, startSelector: nil, stopSelector: nil)
        
        button_repeat_3.isUserInteractionEnabled = false
        perform(#selector(reActive(_:)), with: button_repeat_3, afterDelay: 0.5)
    }
    
    @IBAction func sooarDone(_ sender: Any) {
        playClick()
        var prefs = UserDefaults.standard
        
        
        prefs.set("done", forKey: "soorah-\(sorra)")
        prefs.synchronize()
        
        button_done.isHidden = true
        button_udone.isHidden = false
        
        NotificationCenter.default.post(name: NSNotification.Name("reAlpha"), object: nil)
        
        
    }
    @IBAction func sooaruDone(_ sender: Any) {
        playClick()
        var prefs = UserDefaults.standard
        prefs.set("not", forKey: "soorah-\(sorra)")
        prefs.synchronize()
        
        button_done.isHidden = false
        button_udone.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name("reAlpha"), object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: Notification.Name.myNotificationKey, object: nil)
        
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                
            case 1334:
                print("iPhone 6/6S/7/8")
                viewTopCont.constant = 36
                viewLeftCont.constant = 17
                viewRightCont.constant = 22
                viewBorromCont.constant = 10
                
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                viewTopCont.constant = 42
                viewLeftCont.constant = 25
                viewRightCont.constant = 30
                viewBorromCont.constant = 18
                
            case 2436:
                print("iPhone X/XS/11 Pro")
                viewTopCont.constant = 36
                viewLeftCont.constant = 25
                viewRightCont.constant = 30
                viewBorromCont.constant = 28
                
            case 2688:
                print("iPhone XS Max/11 Pro Max")
                viewTopCont.constant = 38
                viewLeftCont.constant = 29
                viewRightCont.constant = 34
                viewBorromCont.constant = 33
                
            case 1792:
                print("iPhone XR/ 11 ")
                viewTopCont.constant = 40
                viewLeftCont.constant = 30
                viewRightCont.constant = 35
                viewBorromCont.constant = 35
                
            default:
                print("Unknown")
            }
        }
        
        print("Really Screen Height:= \(UIScreen.main.nativeBounds.height)")
        
        if UIDevice().userInterfaceIdiom == .pad {
            switch UIScreen.main.nativeBounds.height {
            case 2048:
                print("iPad Pro (9.7-inch), iPad Air 2, iPad Mini 4")
                viewTopCont.constant = 73
                viewLeftCont.constant = 70
                viewRightCont.constant = -85
                viewBorromCont.constant = -100
                
            case 2732:
                print("Pad Pro (12.9-inch), iPad Pro 12.9-inch (2nd generation)")
                viewTopCont.constant = 95
                viewLeftCont.constant = 95
                viewRightCont.constant = -115
                viewBorromCont.constant = -100
                
                imgTopCont.constant = -120
                imgLeftCont.constant = 20
                imgRightCon.constant = 20
                
                imtTabTopCont.constant = 100
                
            case 1668:
                print("iPad Pro 10.5-inch")
                viewTopCont.constant = 74
                viewLeftCont.constant = 74
                viewRightCont.constant = -90
                viewBorromCont.constant = -100
                
                imgTopCont.constant = -50
                imgLeftCont.constant = 20
                imgRightCon.constant = 20
                
                imtTabTopCont.constant = 100
                
            case 2388:
                print("iPad Pro 11-inch")
                viewTopCont.constant = 75
                viewLeftCont.constant = 80
                viewRightCont.constant = -99
                viewBorromCont.constant = -100
                
                imgTopCont.constant = -30
                imgLeftCont.constant = 20
                imgRightCon.constant = 20
                
            default:
                print("Unknown")
            }
        }
        
        
        viewPage.isOpaque = false
        viewPage.backgroundColor = UIColor.clear
        viewPage.scrollView.showsVerticalScrollIndicator = false
        data_storeV = DB.init()
        
        //let mp3Url = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("Splash_mo01.MP3").absoluteString)
        let mp3Url = Bundle.main.url(forResource: "Splash_mo01", withExtension: "MP3")
        var err: Error?
        
        do {
            player = try AVAudioPlayer(contentsOf: mp3Url!)
        } catch let err {
        }
        NotificationCenter.default.addObserver(self, selector: #selector(setupScroll), name: NSNotification.Name("setupScroll"), object: nil)
        
        button_repeat_1.isHidden = true
        button_repeat_2.isHidden = true
        button_repeat_3.isHidden = true
        
        do {
            click = try AVAudioPlayer(contentsOf: mp3Url!)
        } catch let err {
        }
        
    }
    
    @objc func onNotification(notification:Notification)
    {
        
        isFirstTime = true
        
        image_soora_bgA.isHidden = true
        
        imgGreenFrame.isHidden = true
        viewTextWork.isHidden = true
        
        btnPreL.isHidden = true
        imgBottom.isHidden = true
        btnBack.isHidden = true
        btnNextL.isHidden = true
        bn_play.isHidden = true
        bn_stop.isHidden = true
        button_repeat.isHidden = true
        
        let prefs = UserDefaults.standard
        
        let myString = prefs.string(forKey: "soorah-\(sorra)")
        if (myString == "done") {
            button_done.isHidden = true
            button_udone.isHidden = false
        } else {
            button_done.isHidden = false
            button_udone.isHidden = true
        }
        
        
        LastAnimation()
        
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func setupScroll() {
        
        //    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
        //    [[AVAudioSession sharedInstance] setActive: YES error: nil];
        //    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        
        
        let prefs = UserDefaults.standard
        
        if isFirstTime == true {
            bn_play.isHidden = true
            bn_stop.isHidden = true
            
        } else {
            bn_play.isHidden = false
            bn_stop.isHidden = true
        }
        
        
        button_repeat_1.isHidden = true
        button_repeat_2.isHidden = true
        button_repeat_3.isHidden = true
        
        sorra = Int(prefs.value(forKey: "sorra") as? String ?? "0") ?? 0
        print(sorra)
        sorra_pageNum = Int(data_storeV!.get_sora_pageNum("\(sorra)")) ?? 0
        sorra_pageNum_end = Int(data_storeV!.get_sora_pageNum_end("\(sorra)")) ?? 0
        pagesNum = sorra_pageNum_end - sorra_pageNum + 1
        ayah_count = Int(data_storeV!.get_player_sora_count("\(sorra)")) ?? 0
        print("sorra \(sorra) start \(sorra_pageNum) end \(sorra_pageNum_end) Num \(pagesNum)")
        
        
        
        let myString = prefs.string(forKey: "soorah-\(sorra)")
        
        if isFirstTime == true {
            button_done.isHidden = true
            button_udone.isHidden = true
            
        } else {
            if (myString == "done") {
                button_done.isHidden = true
                button_udone.isHidden = false
            } else {
                button_done.isHidden = false
                button_udone.isHidden = true
            }
        }
        
        
        
        let num = sorra - 50
        if num > 9 {
            image_soora_bgA.image = UIImage(named: "ipad_adnan_bg_\(num).png")
        }else {
            image_soora_bgA.image = UIImage(named: "ipad_adnan_bg_0\(num).png")
        }
        
        
        repeatt = 1
        repeat_seq = 1
        
        current_page = sorra_pageNum
        current_ayah = 0
        updatePage(current_page)
        
        if isFirstTime == false {
            perform(#selector(goPlay(_:)), with: nil, afterDelay: 0.3)
        } else {
            testPNG()
        }
    }
    
    func updatePage(_ page: Int) {
        viewPage.isOpaque = false
        viewPage.backgroundColor = UIColor.clear
        
        print("\(data_storeV!.get_page("\(page)", sorra: "\(sorra)") ?? "")")
        viewPage.loadHTMLString("\(data_storeV!.get_page("\(page)", sorra: "\(sorra)") ?? "")", baseURL: URL(fileURLWithPath: Bundle.main.bundlePath))
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if player!.isPlaying {
            hoverSet()
        }
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        
        if navigationType == .linkClicked {
            if request.url?.absoluteString.hasPrefix("quran://") ?? false {
                let s = request.url?.absoluteString
                let Mid = s?.replacingOccurrences(of: "quran://", with: "")
                let match = "-"
                var sora_num: NSString?
                var ayah_num: String?
                
                let scanner = Scanner(string: Mid ?? "")
                scanner.scanUpTo(match, into: &sora_num)
                
                
                scanner.scanString(match, into: nil)
                ayah_num = (Mid as NSString?)?.substring(from: scanner.scanLocation)
                
                
                print("sora_num: \(sora_num ?? "")")
                print("ayah_num: \(ayah_num ?? "")")
                
                
                return false
            }
        }
        
        
        
        return true
    }
    
    func testPNG() {
        
        UIApplication.shared.isIdleTimerDisabled = true
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        
        var sel_sora: String?
        var sel_ayah: String?
        
        if sorra < 10 {
            sel_sora = String(format: "00%i", sorra)
        } else if sorra < 100 {
            sel_sora = String(format: "0%i", sorra)
        } else {
            sel_sora = String(format: "%i", sorra)
        }
        
        
        if current_ayah < 10 {
            sel_ayah = String(format: "00%i", current_ayah)
        } else if current_ayah < 100 {
            sel_ayah = String(format: "0%i", current_ayah)
        } else {
            sel_ayah = String(format: "%i", current_ayah)
        }
        
        let newPage = Int(data_storeV!.get_player_page(Int32(sorra), Int32(current_ayah)))
        if current_page != newPage {
            current_page = newPage!
            updatePage(current_page)
        }
        
        print("file is : \("\(sel_sora ?? "")\(sel_ayah ?? "").mp3")")
        print("image is : \("\(sel_sora ?? "")\(sel_ayah ?? "").png")")
        
        self.imgChange.image = UIImage(named: "\(sel_sora ?? "")\(sel_ayah ?? "").png")
        
        hoverSet()
        
    }
    
    func playerStart() {
        player!.stop()
        
        bn_play.isHidden = true
        bn_stop.isHidden = false
        
        UIApplication.shared.isIdleTimerDisabled = true
        NotificationCenter.default.post(name: NSNotification.Name("updateToush"), object: nil)
        
        var sel_sora: String?
        var sel_ayah: String?
        
        if sorra < 10 {
            sel_sora = String(format: "00%i", sorra)
        } else if sorra < 100 {
            sel_sora = String(format: "0%i", sorra)
        } else {
            sel_sora = String(format: "%i", sorra)
        }
        
        
        if current_ayah < 10 {
            sel_ayah = String(format: "00%i", current_ayah)
        } else if current_ayah < 100 {
            sel_ayah = String(format: "0%i", current_ayah)
        } else {
            sel_ayah = String(format: "%i", current_ayah)
        }
        
        let newPage = Int(data_storeV!.get_player_page(Int32(sorra), Int32(current_ayah)))
        if current_page != newPage {
            current_page = newPage!
            updatePage(current_page)
        }
        
        print("file is : \("\(sel_sora ?? "")\(sel_ayah ?? "").mp3")")
        print("image is : \("\(sel_sora ?? "")\(sel_ayah ?? "").png")")
        
        self.imgChange.image = UIImage(named: "\(sel_sora ?? "")\(sel_ayah ?? "").png")
        
        hoverSet()
        
        var musicFile: URL?
        if current_ayah == 0 {
            //musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("basmalahforall.mp3").absoluteString)
            musicFile = Bundle.main.url(forResource: "basmalahforall", withExtension: "mp3")
        } else {
            //musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("\(sel_sora ?? "")\(sel_ayah ?? "").mp3").absoluteString)
            musicFile = Bundle.main.url(forResource: "\(sel_sora ?? "")\(sel_ayah ?? "")", withExtension: "mp3")
        }
        
        
        if let musicFile = musicFile {
            do {
                player = try AVAudioPlayer(contentsOf: musicFile)
            }catch let err {
                
            }
            //player.init(contentsOf: musicFile, error: nil)
        }
        player!.delegate = self
        player!.play()
    }
    
    
    func hoverSet() {
        var current_ayahX = current_ayah
        if current_ayah == 0 {
            current_ayahX = 1
        }
        let length = Int(viewPage.stringByEvaluatingJavaScript(from: String(format: "document.getElementsByClassName('quran-%i-%i').length", sorra, current_ayahX)) ?? "") ?? 0
        
        for i in 0..<length {
            
            if current_ayah == 0 {
                viewPage.stringByEvaluatingJavaScript(from: String(format: "document.getElementsByClassName('quran-%i-%i')[%i].className += ' ayahH1';", sorra, current_ayahX, i))
            } else {
                viewPage.stringByEvaluatingJavaScript(from: String(format: "document.getElementsByClassName('quran-%i-%i')[%i].className += ' ayahH';", sorra, current_ayahX, i))
            }
            
            
        }
        
        if current_ayah == 0 {
            if UIDevice.current.userInterfaceIdiom == .pad {
                viewPage.stringByEvaluatingJavaScript(from: "window.scrollTo(0, document.querySelector(\".ayahH1\").offsetTop-180);")
            } else {
                viewPage.stringByEvaluatingJavaScript(from: "window.scrollTo(0, document.querySelector(\".ayahH1\").offsetTop-95);")
            }
        } else {
            if current_ayahX == 1 {
                if UIDevice.current.userInterfaceIdiom == .pad {
                    viewPage.stringByEvaluatingJavaScript(from: "window.scrollTo(0, document.querySelector(\".ayahH\").offsetTop-180);")
                } else {
                    viewPage.stringByEvaluatingJavaScript(from: "window.scrollTo(0, document.querySelector(\".ayahH\").offsetTop-95);")
                }
                
            } else {
                viewPage.stringByEvaluatingJavaScript(from: "window.scrollTo(0, document.querySelector(\".ayahH\").offsetTop);")
            }
        }
        
        
        
        
        //    [viewPage stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementById(\".ayahH\").scrollIntoView()"]];
        
    }
    
    func audioPlayerDidFinishPlaying(_ playerM: AVAudioPlayer, successfully flag: Bool) {
        
        if playerM == player {
            UIApplication.shared.isIdleTimerDisabled = false
            playerStartNext()
        }
    }
    
    func playerStartNext() {
        
        rest()
        
        print("repeatt = \(repeatt)")
        print("repeat_seq = \(repeat_seq)")
        print("current_ayah = \(current_ayah)")
        print("ayah_count = \(ayah_count)")
        
        if isRealRepeatFinished3 == true {
            
            if repeat_seq == 2 {
                isRealRepeatFinished3 = false
                button_repeat.setImage(UIImage(named: "ic_Repet1.png"), for: .normal)
                
            } else if repeat_seq == 1 {
                button_repeat.setImage(UIImage(named: "ic_Repet2.png"), for: .normal)
            }
            
        }
        
        if isRealRepeatFinished2 == true {
            
            if repeat_seq == 1 {
                isRealRepeatFinished2 = false
                button_repeat.setImage(UIImage(named: "ic_Repet1.png"), for: .normal)
            }
            
        }
        
        if repeatt > repeat_seq {
            
            repeat_seq = repeat_seq + 1
            isRepeatFinished = true
            playerStart()
        } else if current_ayah < ayah_count {
            if isRepeatFinished == true {
                repeatt = 1
                repeat_seq = 1
                isRealRepeatFinished1 = false
                isRealRepeatFinished2 = false
                isRealRepeatFinished3 = false
                button_repeat.setImage(UIImage(named: "iphone_repeat_off.png"), for: .normal)
            }
            repeat_seq = 1
            current_ayah = current_ayah + 1
            
            playerStart()
        }
    }
    
    func playerStartPrev() {
        rest()
        if current_ayah > 0 {
            current_ayah = current_ayah - 1
            playerStart()
        }
    }
    
    func rest() {
        
        if isFirstTime == true {
            bn_play.isHidden = true
            bn_stop.isHidden = true
            
        } else {
            bn_play.isHidden = false
            bn_stop.isHidden = true
            
        }
        
        let length = Int(viewPage.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('ayahprev').length") ?? "") ?? 0
        for i in 0..<length {
            let js = String(format: "document.getElementsByClassName('ayahprev')[%i].className", i)
            var clas = viewPage.stringByEvaluatingJavaScript(from: js)
            clas = clas?.replacingOccurrences(of: "ayahH", with: "")
            viewPage.stringByEvaluatingJavaScript(from: String(format: "document.getElementsByClassName('ayahprev')[%i].className = '%@';", i, clas ?? ""))
        }
    }
    
    func playClick() {
        //let musicFile = URL(fileURLWithPath: URL(fileURLWithPath: Bundle.main.resourcePath ?? "").appendingPathComponent("click.mp3").absoluteString)
        let musicFile = Bundle.main.url(forResource: "click", withExtension: "mp3")
        
        do {
            click = try AVAudioPlayer(contentsOf: musicFile!)
        }catch let err {
            
        }
        
        //click.init(contentsOf: musicFile, error: nil)
        click!.play()
    }
    
    
}
