//
//  list_cell.swift
//  Adnan
//
//  Created by Chintan Patel on 23/11/19.
//  Copyright © 2019 Chintan Patel. All rights reserved.
//

import UIKit

class list_cell: UITableViewCell {
    
    @IBOutlet var img_bg_4 : UIImageView!
    @IBOutlet var lb_time : UILabel!
    @IBOutlet var lb_txt : UILabel!
    @IBOutlet var lb_count : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
