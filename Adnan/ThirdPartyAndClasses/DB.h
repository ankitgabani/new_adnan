//
//  DB.h
//  womanIPad
//
//  Created by talal lababidi on 7/1/11.
//  Copyright 2011 zadcom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIDevice+Resolutions.h"

@interface DB : NSObject {
    NSMutableArray *NodesIDs    ;
    NSMutableArray *NodesTitle  ;
    NSMutableArray *NodesTeaser ;
    NSMutableArray *NodesBody   ;
    NSMutableArray *NodesImage  ;
    NSMutableArray *NodesVideo  ;
    NSMutableArray *NodesSound  ;
    NSMutableArray *NodesPdf    ;
}

@property (retain,nonatomic) NSMutableArray *NodesIDs    ;
@property (retain,nonatomic) NSMutableArray *NodesTitle  ;
@property (retain,nonatomic) NSMutableArray *NodesTeaser ;
@property (retain,nonatomic) NSMutableArray *NodesBody   ;
@property (retain,nonatomic) NSMutableArray *NodesImage  ;
@property (retain,nonatomic) NSMutableArray *NodesVideo  ;
@property (retain,nonatomic) NSMutableArray *NodesSound  ;
@property (retain,nonatomic) NSMutableArray *NodesPdf    ;

-(NSString *)nid_max;
-(NSString *)nid_max2;
-(NSString *)nid_max2New;

-(void) insert_data:(NSString *) nid
              title:(NSString *) title
             teaser:(NSString *) teaser
               body:(NSString *) body
               type:(NSString *) type
               term:(NSString *) term
              image:(NSString *) image
              video:(NSString *) video
              sound:(NSString *) sound
                pdf:(NSString *) pdf
               date:(NSString *) date
             change:(NSString *) change;
-(void) get_type:(NSString *) type where:(NSString *) where;


-(void)updateNid:(NSString *) nid;

-(void)updateTitle:(NSString *) nid title:(NSString *) title;


-(NSString *)get_sora_pageNum:(NSString *) sora_num;
-(NSString *)get_sora_pageNum_end:(NSString *) sora_num;
-(NSString *)get_page:(NSString *) page_num sorra:(NSString *)sorra;
-(NSString *)get_player_sora_count:(NSString *) sorra;
-(NSString *)get_player_page:(int ) sora_num :(int ) ayah_num;
-(void)updateNid_sound_remove:(NSString *) nid;
@end
