//
//  DB.m
//  charityIPad
//
//  Created by talal lababidi on 7/1/11.
//  Copyright 2011 zadcom. All rights reserved.
//

#import "DB.h"
//#include <sqlite/sqlite.h>
//#import "sqlite/sqlite3.c"
#import <sqlite3.h>
#include <sys/xattr.h>
//#import <sqlite/sqlite.h>

@implementation DB

@synthesize NodesIDs    ;
@synthesize NodesTitle  ;
@synthesize NodesTeaser ;
@synthesize NodesBody   ;
@synthesize NodesImage  ;
@synthesize NodesVideo  ;
@synthesize NodesSound  ;
@synthesize NodesPdf    ;



-(NSString *)nid_max{
    NSString *val ;
    val = @"0";
    
    
    NSLog(@"max hhheee :%@",val);
    
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    int result = sqlite3_open([dbPath UTF8String] , &database );
    if(result == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        
        NSString *sql_st = [NSString stringWithFormat:@"select max(change) from node "];
        
        
        //NSLog(@"query %@",sql_st);
        sqlite3_prepare_v2(database, [sql_st UTF8String], -1, &statement, nil);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            val = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            
        }
        //NSLog(@"error = %s", sqlite3_errmsg(database));
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
    //NSLog(@"done select");
    NSLog(@"max val :%@",val);
    return val;
}
-(NSString *)nid_max2{
    NSString *val ;
    val = @"0";
    
    
    NSLog(@"max hhheee :%@",val);
    
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    int result = sqlite3_open([dbPath UTF8String] , &database );
    if(result == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        
        NSString *sql_st = [NSString stringWithFormat:@"select max(nid) from node2 "];
        
        
        //NSLog(@"query %@",sql_st);
        sqlite3_prepare_v2(database, [sql_st UTF8String], -1, &statement, nil);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            val = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            
        }
        //NSLog(@"error = %s", sqlite3_errmsg(database));
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
    //NSLog(@"done select");
    NSLog(@"max val :%@",val);
    return val;
}
-(NSString *)nid_max2New{
    NSString *val ;
    val = @"0";
    
    
    NSLog(@"max hhheee :%@",val);
    
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    int result = sqlite3_open([dbPath UTF8String] , &database );
    if(result == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        
        NSString *sql_st = [NSString stringWithFormat:@"select max(nid) from node2 "];
        
        
        //NSLog(@"query %@",sql_st);
        sqlite3_prepare_v2(database, [sql_st UTF8String], -1, &statement, nil);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            val = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            
        }
        //NSLog(@"error = %s", sqlite3_errmsg(database));
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
    //NSLog(@"done select");
    NSLog(@"max val :%@",val);
    return [NSString stringWithFormat:@"%d",[val intValue]+1];
}
-(void)updateNid:(NSString *) nid{
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    int result = sqlite3_open([dbPath UTF8String] , &database );
    if(result == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        
        NSString *sql_st = [NSString stringWithFormat:@"update node set 'show' = 1 where nid = %@ ",nid];
        
        
        NSLog(@"query %@",sql_st);
        sqlite3_prepare_v2(database, [sql_st UTF8String], -1, &statement, nil);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            
            
        }
        NSLog(@"error = %s", sqlite3_errmsg(database));
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
}

-(void) insert_data:(NSString *) nid
              title:(NSString *) title
             teaser:(NSString *) teaser
               body:(NSString *) body
               type:(NSString *) type
               term:(NSString *) term
              image:(NSString *) image
              video:(NSString *) video
              sound:(NSString *) sound
                pdf:(NSString *) pdf
               date:(NSString *) date
             change:(NSString *) change{
    
    sqlite3 *database;
	NSString *dbPath = [self getDBPath];
	int result = sqlite3_open([dbPath UTF8String] , &database );
    
    if(result == SQLITE_OK)
	{
        sqlite3_stmt *statement;

        
        const char *sql;
        if( [type isEqualToString:@"advban"] ){
            sql = "INSERT OR REPLACE  into node(nid,title,teaser,body,type,term,image,video,sound,pdf,date,change,show)  values(?,?,?,?,?,?,?,?,?,?,?,?,0);";
        }
        else{
            sql = "INSERT OR REPLACE  into node2(nid,title,teaser,body,type,term,image,video,sound,pdf,date,change,show)  values(?,?,?,?,?,?,?,?,?,?,?,?,0);";
        }
        
        
        sqlite3_prepare_v2(database, sql, -1, &statement, nil);
        
        const char *sql_text =[nid UTF8String];
        sqlite3_bind_text(statement, 1, sql_text ,-1, SQLITE_TRANSIENT);
        
        sql_text =[title UTF8String];
        sqlite3_bind_text(statement, 2, sql_text ,-1, SQLITE_TRANSIENT);
        
        sql_text =[teaser UTF8String];
        sqlite3_bind_text(statement, 3, sql_text ,-1, SQLITE_TRANSIENT);
        
        sql_text =[body UTF8String];
        sqlite3_bind_text(statement, 4, sql_text ,-1, SQLITE_TRANSIENT);
        
        sql_text =[type UTF8String];
        sqlite3_bind_text(statement, 5, sql_text ,-1, SQLITE_TRANSIENT);
        
        sql_text =[term UTF8String];
        sqlite3_bind_text(statement, 6, sql_text ,-1, SQLITE_TRANSIENT);
        
        sql_text =[image UTF8String];
        sqlite3_bind_text(statement, 7, sql_text ,-1, SQLITE_TRANSIENT);
        
        sql_text =[video UTF8String];
        sqlite3_bind_text(statement, 8, sql_text ,-1, SQLITE_TRANSIENT);
        
        sql_text =[sound UTF8String];
        sqlite3_bind_text(statement, 9, sql_text ,-1, SQLITE_TRANSIENT);
        
        sql_text =[pdf UTF8String];
        sqlite3_bind_text(statement, 10, sql_text ,-1, SQLITE_TRANSIENT);
        
        sql_text =[date UTF8String];
        sqlite3_bind_text(statement, 11, sql_text ,-1, SQLITE_TRANSIENT);
        
        sql_text =[change UTF8String];
        sqlite3_bind_text(statement, 12, sql_text ,-1, SQLITE_TRANSIENT);
        
        
        
        sqlite3_step(statement);
        //NSLog(@"error = %s", sqlite3_errmsg(database));
        sqlite3_finalize(statement);
        
    }
    
    
    
    sqlite3_close(database);
}
-(void) get_type:(NSString *) type where:(NSString *) where{
    
    
    NodesIDs   = [[NSMutableArray alloc] init] ;
    NodesTitle = [[NSMutableArray alloc] init];
    NodesTeaser = [[NSMutableArray alloc] init];
    NodesBody = [[NSMutableArray alloc] init];
    NodesImage = [[NSMutableArray alloc] init];
    NodesVideo = [[NSMutableArray alloc] init];
    NodesSound = [[NSMutableArray alloc] init];
    NodesPdf = [[NSMutableArray alloc] init];
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    int result = sqlite3_open([dbPath UTF8String] , &database );
    if(result == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        
        
        NSString *sql_st ;
        
        if( [type isEqualToString:@"advban"] ){
            sql_st = [NSString stringWithFormat:@"SELECT nid,title,teaser,body,image,video,sound,pdf FROM node where type = '%@' %@ order by date asc,nid asc ",type,where];
        }
        else{
            sql_st = [NSString stringWithFormat:@"SELECT nid,title,teaser,body,image,video,sound,pdf FROM node2 where type = '%@' %@ order by date asc,nid asc ",type,where];
        }

        
        
        NSLog(@"query %@",sql_st);
        sqlite3_prepare_v2(database, [sql_st UTF8String], -1, &statement, nil);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            NSString *nid    =  [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            NSString *title  = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            NSString *teaser = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
            NSString *body   = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
            
            NSString *image = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
            NSString *video = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 5)];
            NSString *sound = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 6)];
            NSString *pdf   = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 7)];
            
            
            
            [NodesIDs   addObject:nid];
            [NodesTitle addObject:title];
            [NodesTeaser addObject:teaser];
            [NodesBody addObject:body];
            [NodesImage addObject:image];
            [NodesVideo addObject:video];
            [NodesSound addObject:sound];
            [NodesPdf addObject:pdf];
            
            
              
        }
//        NSLog(@"error = %s", sqlite3_errmsg(database));
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
    //NSLog(@"done select");
    
    
    
//    [NodesIDs   addObject:@"1"];
//    [NodesTitle addObject:@"1"];
//    [NodesTeaser addObject:@"1"];
//    [NodesBody addObject:@"http://allseeing-i.com/i/add-linked-library.png"];
//    [NodesImage addObject:@"http://allseeing-i.com/i/add-linked-library.png"];
//    [NodesVideo addObject:@"1"];
//    [NodesSound addObject:@"1"];
//    [NodesPdf addObject:@"1"];
    
    
}









- (NSString *) getDBPath{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    return [documentsDir stringByAppendingPathComponent:@"DoNotBackUp/kidsapps.sqlite"];
}
- (NSString *) getDBPath_font:(NSString *)font{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    return [documentsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"DoNotBackUp/fonts/%@.TTF",font]];
    
}


















-(NSString *)get_sora_pageNum:(NSString *) sora_num{
    NSString *val ;
    val = @"0";
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    int result = sqlite3_open([dbPath UTF8String] , &database );
    if(result == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        
        NSString *sql_st = [NSString stringWithFormat:@"select page from data_sora ds where  ds.id = '%@' order by id limit 1 ; ",sora_num];
        
        
        //NSLog(@"query %@",sql_st);
        sqlite3_prepare_v2(database, [sql_st UTF8String], -1, &statement, nil);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            val = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            
        }
        //NSLog(@"error = %s", sqlite3_errmsg(database));
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
    
    
    
    
    
    return val;
}
-(NSString *)get_sora_pageNum_end:(NSString *) sora_num{
    NSString *val ;
    val = @"0";
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    int result = sqlite3_open([dbPath UTF8String] , &database );
    if(result == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        
        NSString *sql_st = [NSString stringWithFormat:@"select id from data_page ds where  ds.soraE = '%@' order by ayaE desc limit 1 ; ",sora_num];
        
        
        //NSLog(@"query %@",sql_st);
        sqlite3_prepare_v2(database, [sql_st UTF8String], -1, &statement, nil);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            val = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            
        }
        //NSLog(@"error = %s", sqlite3_errmsg(database));
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
    
    
    
    
    
    return val;
}
-(NSString *)fixQuran:(NSString *) page_num :(NSString *)val{
    
    
    val = [val stringByReplacingOccurrencesOfString:@"\n\r" withString:@"<br />"];
    val = [val stringByReplacingOccurrencesOfString:@"\\n\\r" withString:@"<br />"];
    
    if( [page_num intValue] == 187 ){
        //////////////////fix
        val = [val stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
        
    }
    
    
    
    val = [val stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"];
    val = [val stringByReplacingOccurrencesOfString:@"\\n" withString:@"<br />"];
    val = [val stringByReplacingOccurrencesOfString:@"</div><br />" withString:@"</div>"];
    val = [val stringByReplacingOccurrencesOfString:@"</span><br />" withString:@"</span>"];
    
    
    
    
    return val;
}
-(NSString *)get_page:(NSString *) page_num sorra:(NSString *)sorra{
    NSString *val ;
    val = @"0";
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    int result = sqlite3_open([dbPath UTF8String] , &database );
    if(result == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        
        NSString *sql_st = [NSString stringWithFormat:@"select ready from data_pages where id ='%@' ",page_num];
        
        
        //NSLog(@"query %@",sql_st);
        sqlite3_prepare_v2(database, [sql_st UTF8String], -1, &statement, nil);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            val = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            
        }
        //NSLog(@"error = %s", sqlite3_errmsg(database));
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
    
    
    
    
    NSString *encodedValue1_font ;
    if( [page_num intValue] < 10 ){
        encodedValue1_font = [NSString stringWithFormat:@"QCF_P00%@",page_num ];
    }
    else if( [page_num intValue] < 100 ){
        encodedValue1_font = [NSString stringWithFormat:@"QCF_P0%@",page_num ];
    }
    else{
        encodedValue1_font = [NSString stringWithFormat:@"QCF_P%@",page_num ];
    }
    
    

    
    val = [self fixQuran:page_num :val];
    //////////////////fix
    
    
    //    NSLog(@"page ::::::::::::::::::::::::::: %@",page_num);
    //    NSLog(@"%@",val);
    //    NSLog(@"page ::::::::::::::::::::::::::: %@",page_num);
    
    
    NSString *styleFile ;
    switch ([UIDevice currentResolution]) {
        case 3:
            //NSLog(@"Iphone 5");
            styleFile = @"styleIPhone5.css";
            break;
        case 4:
        case 5:
            //NSLog(@"Normal Ipad");
            styleFile = @"styleIPAD.css";
            break;
        case 1:
        case 2:
        default:
            //NSLog(@"Def iphone 3 || 4");
            styleFile = @"styleIPhone.css";
            break;
    }
    
    
    NSString * myHTML = [NSString stringWithFormat:@"<html>"
                         "<head>"
                         "<style type=\"text/css\">"
                         "@font-face{font-family: myFirstFontB;src: url('%@')}"
                         "@font-face{font-family: myFirstFont;src: url('%@')}"
                         "</style>"
                         
                         "<link rel='stylesheet' type='text/css' href='%@' /></head>"
                         "<body>"
                         "<div class='page%@'>%@</div>"
                         "</body>"
                         "</html>",[self getDBPath_font:@"QCF_BSML"], [self getDBPath_font:encodedValue1_font],
                         styleFile,([page_num intValue] == 1 || [page_num intValue] == 2 ?@"1":@""),val];
    
    //    NSLog(@"page ::::::::::::::::::::::::::: %@",page_num);
    //    NSLog(@"%@",myHTML);
    //    NSLog(@"page ::::::::::::::::::::::::::: %@",page_num);
    
    //NSLog(@"done select");
    //NSLog(@"max val :%@",val);
    return myHTML;
}


-(NSString *)get_player_sora_count:(NSString *) sorra{
    NSString *val ;
    val = @"0";
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    int result = sqlite3_open([dbPath UTF8String] , &database );
    if(result == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        
        NSString *sql_st = [NSString stringWithFormat:@"select num from data_sora where id = '%@'; ",sorra];
        
        
        //NSLog(@"query %@",sql_st);
        sqlite3_prepare_v2(database, [sql_st UTF8String], -1, &statement, nil);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            val = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            
        }
        //NSLog(@"error = %s", sqlite3_errmsg(database));
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
    
    
    
    return val;
}
-(NSString *)get_player_page:(int ) sora_num :(int ) ayah_num{
    NSString *val ;
    val = @"0";
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    int result = sqlite3_open([dbPath UTF8String] , &database );
    if(result == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        
        NSString *sql_st = [NSString stringWithFormat:@"select id from data_page where soraS = '%i' and ayaS <= '%i' order by id desc limit 1; ",sora_num,ayah_num];
        
        
        NSLog(@"query %@",sql_st);
        sqlite3_prepare_v2(database, [sql_st UTF8String], -1, &statement, nil);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            val = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            
        }
        //NSLog(@"error = %s", sqlite3_errmsg(database));
        sqlite3_finalize(statement);
        
        
        
        
        
        if( [val isEqualToString:@"0"] ){
            sqlite3_stmt *statement;
            
            
            NSString *sql_st = [NSString stringWithFormat:@"select page from data_sora where id = '%i' order by id desc limit 1; ",sora_num];
            
            
            NSLog(@"query %@",sql_st);
            sqlite3_prepare_v2(database, [sql_st UTF8String], -1, &statement, nil);
            
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                val = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                
            }
            //NSLog(@"error = %s", sqlite3_errmsg(database));
            sqlite3_finalize(statement);
        }
        
        
    }
    sqlite3_close(database);
    
    
    
    
    
    return val;
}




-(void)updateNid_sound_remove:(NSString *) nid{
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    int result = sqlite3_open([dbPath UTF8String] , &database );
    if(result == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        
        NSString *sql_st = [NSString stringWithFormat:@"delete from  node2  where nid = %@ ",nid];
        
        
        NSLog(@"query %@",sql_st);
        sqlite3_prepare_v2(database, [sql_st UTF8String], -1, &statement, nil);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            
            
        }
        NSLog(@"error = %s", sqlite3_errmsg(database));
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
}


-(void)updateTitle:(NSString *) nid title:(NSString *) title{
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    int result = sqlite3_open([dbPath UTF8String] , &database );
    if(result == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        
        NSString *sql_st = [NSString stringWithFormat:@"update node2 set 'title' = '%@' where nid = %@ ",title,nid];
        
        
        NSLog(@"query %@",sql_st);
        sqlite3_prepare_v2(database, [sql_st UTF8String], -1, &statement, nil);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            
            
        }
        NSLog(@"error = %s", sqlite3_errmsg(database));
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
}


@end
