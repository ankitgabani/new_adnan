//
//  installer.m
//  kidsapps
//
//  Created by talal lababidi on 5/18/13.
//  Copyright (c) 2013 talal lababidi. All rights reserved.
//

#import "installer.h"
#import "ZipArchive.h"
#import "Adnan-Swift.h"
#include <sys/xattr.h>


#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)



@interface installer ()

@end

@implementation installer


AppDelegate *deleg;
bool iOS52 = false;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    deleg = [[AppDelegate alloc] init];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)startCopy{
    [self  performSelector:@selector(copyDatabaseIfNeeded)	withObject:nil afterDelay:0.0];
    [self  performSelectorInBackground:@selector(copyDatabaseIfNeeded)	withObject:nil];
}







////********************************////
////********************************////
////********************************////
#pragma mark - Database
////********************************////
////********************************////
////********************************////


- (void) copyDatabaseIfNeeded {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"5.0.1"))
        iOS52 = YES;
    
    
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/DoNotBackUp"];
    NSURL *storepath = [NSURL fileURLWithPath:path];
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:nil];
        // Set do not backup attribute to whole folder
        if (iOS52) {
            BOOL success = [self addSkipBackupAttributeToItemAtURL:storepath];
            if (success)
                NSLog(@"Marked %@", path);
            else
                NSLog(@"Can't marked %@", path);
        }
    }
    
    
    
    
    
    //Using NSFileManager we can perform many file system operations.
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSString *dbPath = [self getDBPath];
    BOOL success = [fileManager fileExistsAtPath:dbPath];
    
    if(!success) {
        
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"kidsapps.sqlite"];
        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        NSLog(@"Database file copied from bundle to %@", dbPath);
        if (success)
        {
            if (iOS52) {
                NSURL *storeUrl = [NSURL fileURLWithPath:dbPath];
                BOOL success = [self addSkipBackupAttributeToItemAtURL:storeUrl];
                if (success)
                    NSLog(@"Marked %@", dbPath);
                else
                    NSLog(@"Can't marked %@", dbPath);
            }
            [prefs setObject:@"done" forKey:@"dbCopy"];
            [prefs synchronize];
            
        }else
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
        
        
    } else {
        
        NSLog(@"Database file found at path %@", dbPath);
        
    }
    
    [self  performSelector:@selector(copyFontsIfNeeded)	withObject:nil afterDelay:0.0];
//    [self  performSelectorInBackground:@selector(copyFontsIfNeeded)	withObject:nil];
    
    
}










////********************************////
////********************************////
////********************************////
#pragma mark -fonts
////********************************////
////********************************////
////********************************////


- (void) copyFontsIfNeeded {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"5.0.1"))
        iOS52 = YES;
    
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/DoNotBackUp"];
    NSURL *storepath = [NSURL fileURLWithPath:path];
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:nil];
        // Set do not backup attribute to whole folder
        if (iOS52) {
            BOOL success = [self addSkipBackupAttributeToItemAtURL:storepath];
            if (success)
                NSLog(@"Marked %@", path);
            else
                NSLog(@"Can't marked %@", path);
        }
    }
    
    
    
    
    
    //Using NSFileManager we can perform many file system operations.
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSString *dbPath = [self getFontsPath];
    BOOL success = [fileManager fileExistsAtPath:dbPath];
    
    if(!success) {
        
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"fonts.zip"];
        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        NSLog(@"fonts file copied from bundle to %@", dbPath);
        if (success)
        {
            if (iOS52) {
                NSURL *storeUrl = [NSURL fileURLWithPath:dbPath];
                BOOL success = [self addSkipBackupAttributeToItemAtURL:storeUrl];
                if (success)
                    NSLog(@"Marked %@", dbPath);
                else
                    NSLog(@"Can't marked %@", dbPath);
            }
            [prefs setObject:@"done" forKey:@"fontsCopy"];
            [prefs synchronize];
        }else
            NSAssert1(0, @"Failed to create writable fonts file with message '%@'.", [error localizedDescription]);
        
        
        
    } else {
        
        NSLog(@"fonts file found at path %@", dbPath);
        
        
    }
    
    
    NSString *st = [prefs stringForKey:@"fontUnZip"];
    if( ![st isEqualToString:@"done"] ){
        [self  performSelector:@selector(unZipFonts)	withObject:nil afterDelay:0.0];
//        [self  performSelectorInBackground:@selector(unZipFonts)	withObject:nil];
    }
    else{
         [[NSNotificationCenter defaultCenter] postNotificationName:@"prepDataEnd" object:nil];
    }
    
    
    
    
    
    
	
}
-(void)unZipFonts {
    NSString *fontPath = [self getFontsPath];
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/DoNotBackUp"];
    
    ZipArchive* Files = [[ZipArchive alloc] init];
    if( [Files UnzipOpenFile:fontPath] ) {
        
        if( [Files UnzipFileTo:path overWrite:YES] == NO )
        {
            NSLog(@"error");
        }
        [Files UnzipCloseFile];

        
    } else {
        
    }
    
    NSLog(@"done");
    
    
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:@"done" forKey:@"fontUnZip"];
    [prefs synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"prepDataEnd" object:nil];
//    [self performSelectorOnMainThread:@selector(prepDataEnd) withObject:nil waitUntilDone:NO];
}














- (NSString *) getDBPath{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    return [documentsDir stringByAppendingPathComponent:@"DoNotBackUp/kidsapps.sqlite"];
}
- (NSString *) getFontsPath{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    return [documentsDir stringByAppendingPathComponent:@"DoNotBackUp/fonts.zip"];
}
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    const char* filePath = [[URL path] fileSystemRepresentation];
    
    const char* attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;
    
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    return result == 0;
    
}


@end
