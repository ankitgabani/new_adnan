//
//  updater.m
//  jawalzad
//
//  Created by طللولتي Lababidi on 1/15/12.
//  Copyright (c) 2012 zadcom. All rights reserved.
//

#import "updater.h"
#import "ASIFormDataRequest.h"
#import <CommonCrypto/CommonDigest.h>
#import "DB.h"
#import "JSON.h"



@implementation updater

#define DEST_PATH	[NSHomeDirectory() stringByAppendingString:@"/Documents/DoNotBackUp/"]

DB *data_storeU;
bool sending = false;
NSMutableArray *DefsSignUp;

int conn_timeout = 10;

- (void)updater{
    data_storeU = [[DB alloc]init];
    
	NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.kidsapp.sa/kidsapp.sa/ijson?changed=%@",[data_storeU nid_max]]];
//    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://drsamisalman.com/app/ijson?changed=%@",[data_storeU nid_max]]];
    
	ASIFormDataRequest* request = [ASIFormDataRequest requestWithURL:url];
	[request setDelegate:self];
    
    [request setDidFailSelector:@selector(updater_done2_fail:)];
    [request setDidFinishSelector:@selector(updater_done:)];
    
    
    [request startAsynchronous];
}
- (void)updater_done2_fail:(ASIHTTPRequest *)request{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"do_update_done" object:nil];
}
- (void)updater_done:(ASIHTTPRequest *)request{
    data_storeU = [[DB alloc]init];
    NSString *response = [request responseString];
    
    
    
    NSError *error;
	SBJSON *json = [SBJSON new];
	NSArray *newItems = [json objectWithString:response error:&error];
	
    
    
    for (NSDictionary *newItem in newItems)
    {
        //NSLog(@"nid : %@",[newItem objectForKey:@"nid"]);
        
        [data_storeU insert_data:[newItem objectForKey:@"nid"]
                           title:[newItem objectForKey:@"title"]
                          teaser:[newItem objectForKey:@"teaser"]
                            body:[newItem objectForKey:@"body"]
                            type:[newItem objectForKey:@"type"]
                            term:[newItem objectForKey:@"term"]
                           image:[newItem objectForKey:@"image"]
                           video:[newItem objectForKey:@"video"]
                           sound:[newItem objectForKey:@"sound"]
                             pdf:[newItem objectForKey:@"pdf"]
                            date:[newItem objectForKey:@"date"]
                          change:[newItem objectForKey:@"changed"]
         ];
        
        
    }
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"do_update_done" object:nil];
}






















- (void)contacts:(NSString *)name title:(NSString *)title msg:(NSString *)msg{
    
    NSURL* url = [NSURL URLWithString:@"http://www.kidsapp.sa/kidsapp.sa/contacts/insert"];

    
	ASIFormDataRequest* request = [ASIFormDataRequest requestWithURL:url];
    [request setShouldAttemptPersistentConnection:NO];
    [request setTimeOutSeconds:conn_timeout];
	[request setDelegate:self];

    
    [request setPostValue:title forKey:@"name"];
    [request setPostValue:title forKey:@"title"];
    [request setPostValue:@"عدنان 2" forKey:@"app"];
    [request setPostValue:msg forKey:@"msg"];
    

    
    
    [request setDidFinishSelector:@selector(contacts_done:)];
    [request setDidFailSelector:@selector(contacts_ndone:)];
    [request startAsynchronous];
}


- (void)contacts_done:(ASIHTTPRequest *)request{
    NSString *response = [request responseString];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"do_contacts_done" object:response];
    
    
    
}
- (void)contacts_ndone:(ASIHTTPRequest *)request{
    NSString *response = [request responseString];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"do_contacts_donec" object:response];
    
}












@end
