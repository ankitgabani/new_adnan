//
//  list.swift
//  Adnan
//
//  Created by Chintan Patel on 22/11/19.
//  Copyright © 2019 Chintan Patel. All rights reserved.
//

extension Notification.Name {
    
    public static let myNotificationKey = Notification.Name(rawValue: "myNotificationKey")
}

import UIKit

class list: UIViewController {

    @IBOutlet var button_1 : UIButton!
    @IBOutlet var button_2 : UIButton!
    @IBOutlet var button_3 : UIButton!
    @IBOutlet var button_4 : UIButton!
    @IBOutlet var button_5 : UIButton!
    @IBOutlet var button_6 : UIButton!
    @IBOutlet var button_7 : UIButton!
    @IBOutlet var button_8 : UIButton!
    @IBOutlet var button_9 : UIButton!
    @IBOutlet var button_10 : UIButton!
    @IBOutlet var button_11 : UIButton!
    @IBOutlet var button_12 : UIButton!
    @IBOutlet var button_13 : UIButton!
    @IBOutlet var button_14 : UIButton!
    @IBOutlet var button_15 : UIButton!
    @IBOutlet var button_16 : UIButton!
    @IBOutlet var button_17 : UIButton!
    @IBOutlet var button_18 : UIButton!
    @IBOutlet var button_19 : UIButton!
    @IBOutlet var button_20 : UIButton!
    @IBOutlet var button_21 : UIButton!
    @IBOutlet var button_22 : UIButton!
    @IBOutlet var button_23 : UIButton!
    @IBOutlet var button_24 : UIButton!
    @IBOutlet var button_25 : UIButton!
    @IBOutlet var button_26 : UIButton!
    @IBOutlet var button_27 : UIButton!
    
    
    @IBAction func goPage(_ sender: Any) {
        let theButton = sender as? UIButton
        
        
        var prefs = UserDefaults.standard
        prefs.set("\(Int(theButton?.tag ?? 0))", forKey: "sorra")
        prefs.synchronize()
        
        
        print("sorra num \(Int(theButton?.tag ?? 0))")
        
        NotificationCenter.default.post(name: Notification.Name.myNotificationKey, object: nil, userInfo:["text": "GOTOFOR"]) // Notification

        
        NotificationCenter.default.post(name: NSNotification.Name("gotoViewView"), object: nil)
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var startInt: Float = 1
    
    @IBAction func goBackHome(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name("gotoHome"), object: nil)
        hideAll()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideAll()
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(view_1), name: NSNotification.Name("view_1"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(view_2), name: NSNotification.Name("view_2"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(view_3), name: NSNotification.Name("view_3"), object: nil)
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(reAlpha), name: NSNotification.Name("reAlpha"), object: nil)
    }
    
    func reAlphaCK(_ sorra: String?) -> Bool {
        let prefs = UserDefaults.standard
        let myString = prefs.string(forKey: "soorah-\(sorra ?? "")")
        if (myString == "done") {
            return true
        }
        return false
        
    }
    
    @objc func reAlpha() {
        button_1.alpha = 1
        button_2.alpha = 1
        button_3.alpha = 1
        button_4.alpha = 1
        button_5.alpha = 1
        button_6.alpha = 1
        button_7.alpha = 1
        button_8.alpha = 1
        button_9.alpha = 1
        button_10.alpha = 1
        button_11.alpha = 1
        button_12.alpha = 1
        button_13.alpha = 1
        button_14.alpha = 1
        button_15.alpha = 1
        button_16.alpha = 1
        button_17.alpha = 1
        button_18.alpha = 1
        button_19.alpha = 1
        button_20.alpha = 1
        button_21.alpha = 1
        button_22.alpha = 1
        button_23.alpha = 1
        button_24.alpha = 1
        button_25.alpha = 1
        button_26.alpha = 1
        button_27.alpha = 1
        
        
        if reAlphaCK("51") {
            button_1.alpha = 0.5
        }
        if reAlphaCK("52") {
            button_2.alpha = 0.5
        }
        if reAlphaCK("53") {
            button_3.alpha = 0.5
        }
        if reAlphaCK("54") {
            button_4.alpha = 0.5
        }
        if reAlphaCK("55") {
            button_5.alpha = 0.5
        }
        if reAlphaCK("56") {
            button_6.alpha = 0.5
        }
        if reAlphaCK("57") {
            button_7.alpha = 0.5
        }
        if reAlphaCK("58") {
            button_8.alpha = 0.5
        }
        if reAlphaCK("59") {
            button_9.alpha = 0.5
        }
        if reAlphaCK("60") {
            button_10.alpha = 0.5
        }
        if reAlphaCK("61") {
            button_11.alpha = 0.5
        }
        if reAlphaCK("62") {
            button_12.alpha = 0.5
        }
        if reAlphaCK("63") {
            button_13.alpha = 0.5
        }
        if reAlphaCK("64") {
            button_14.alpha = 0.5
        }
        if reAlphaCK("65") {
            button_15.alpha = 0.5
        }
        if reAlphaCK("66") {
            button_16.alpha = 0.5
        }
        if reAlphaCK("67") {
            button_17.alpha = 0.5
        }
        if reAlphaCK("68") {
            button_18.alpha = 0.5
        }
        if reAlphaCK("69") {
            button_19.alpha = 0.5
        }
        if reAlphaCK("70") {
            button_20.alpha = 0.5
        }
        if reAlphaCK("71") {
            button_21.alpha = 0.5
        }
        if reAlphaCK("72") {
            button_22.alpha = 0.5
        }
        if reAlphaCK("73") {
            button_23.alpha = 0.5
        }
        if reAlphaCK("74") {
            button_24.alpha = 0.5
        }
        if reAlphaCK("75") {
            button_25.alpha = 0.5
        }
        if reAlphaCK("76") {
            button_26.alpha = 0.5
        }
        if reAlphaCK("77") {
            button_27.alpha = 0.5
        }
    }
    
    func hideAll(){
        button_1.isHidden = true
        button_2.isHidden = true
        button_3.isHidden = true
        button_4.isHidden = true
        button_5.isHidden = true
        button_6.isHidden = true
        button_7.isHidden = true
        button_8.isHidden = true
        button_9.isHidden = true
        button_10.isHidden = true
        button_11.isHidden = true
        button_12.isHidden = true
        button_13.isHidden = true
        button_14.isHidden = true
        button_15.isHidden = true
        button_16.isHidden = true
        button_17.isHidden = true
        button_18.isHidden = true
        button_19.isHidden = true
        button_20.isHidden = true
        button_21.isHidden = true
        button_22.isHidden = true
        button_23.isHidden = true
        button_24.isHidden = true
        button_25.isHidden = true
        button_26.isHidden = true
        button_27.isHidden = true
    }
    
    @objc func view_1() {
        reAlpha()
        view.isUserInteractionEnabled = false
        let start = startInt
        let interval: Float = 0.1
        perform(#selector(viewButton(_:)), with: button_1, afterDelay: TimeInterval(start))
        perform(#selector(viewButton(_:)), with: button_2, afterDelay: TimeInterval(start + interval))
        perform(#selector(viewButton(_:)), with: button_3, afterDelay: TimeInterval(start + interval * 2))
        perform(#selector(viewButton(_:)), with: button_4, afterDelay: TimeInterval(start + interval * 3))
        perform(#selector(viewButton(_:)), with: button_5, afterDelay: TimeInterval(start + interval * 4))
        perform(#selector(viewButton(_:)), with: button_6, afterDelay: TimeInterval(start + interval * 5))
        perform(#selector(viewButton(_:)), with: button_7, afterDelay: TimeInterval(start + interval * 6))
        
        
        
        perform(#selector(viewActive), with: nil, afterDelay: TimeInterval(start + interval * 11))
    }
    
    @objc func view_2() {
        reAlpha()
        
        view.isUserInteractionEnabled = false
        let start = startInt
        let interval: Float = 0.1
        perform(#selector(viewButton(_:)), with: button_8, afterDelay: TimeInterval(start))
        perform(#selector(viewButton(_:)), with: button_9, afterDelay: TimeInterval(start + interval))
        perform(#selector(viewButton(_:)), with: button_10, afterDelay: TimeInterval(start + interval * 2))
        perform(#selector(viewButton(_:)), with: button_11, afterDelay: TimeInterval(start + interval * 3))
        perform(#selector(viewButton(_:)), with: button_12, afterDelay: TimeInterval(start + interval * 4))
        perform(#selector(viewButton(_:)), with: button_13, afterDelay: TimeInterval(start + interval * 5))
        perform(#selector(viewButton(_:)), with: button_14, afterDelay: TimeInterval(start + interval * 6))
        perform(#selector(viewButton(_:)), with: button_15, afterDelay: TimeInterval(start + interval * 7))
        perform(#selector(viewButton(_:)), with: button_16, afterDelay: TimeInterval(start + interval * 8))
        
        perform(#selector(viewActive), with: nil, afterDelay: TimeInterval(start + interval * 11))
    }
    
    @objc func view_3() {
        reAlpha()
        view.isUserInteractionEnabled = false
        let start = startInt
        let interval: Float = 0.1
        perform(#selector(viewButton(_:)), with: button_17, afterDelay: TimeInterval(start))
        perform(#selector(viewButton(_:)), with: button_18, afterDelay: TimeInterval(start + interval))
        perform(#selector(viewButton(_:)), with: button_19, afterDelay: TimeInterval(start + interval * 2))
        perform(#selector(viewButton(_:)), with: button_20, afterDelay: TimeInterval(start + interval * 3))
        perform(#selector(viewButton(_:)), with: button_21, afterDelay: TimeInterval(start + interval * 4))
        perform(#selector(viewButton(_:)), with: button_22, afterDelay: TimeInterval(start + interval * 5))
        perform(#selector(viewButton(_:)), with: button_23, afterDelay: TimeInterval(start + interval * 6))
        perform(#selector(viewButton(_:)), with: button_24, afterDelay: TimeInterval(start + interval * 7))
        perform(#selector(viewButton(_:)), with: button_25, afterDelay: TimeInterval(start + interval * 8))
        perform(#selector(viewButton(_:)), with: button_26, afterDelay: TimeInterval(start + interval * 9))
        perform(#selector(viewButton(_:)), with: button_27, afterDelay: TimeInterval(start + interval * 10))
        
        
        perform(#selector(viewActive), with: nil, afterDelay: TimeInterval(start + interval * 11))
    }
    
    @objc func viewButton(_ bn: UIButton?) {
        bn?.fade(in: 0.4, delegate: nil)
    }
    
    @objc func viewActive() {
        view.isUserInteractionEnabled = true
    }
    
}
