//
//  SlashViewController.swift
//  Adnan
//
//  Created by Gabani on 01/02/20.
//  Copyright © 2020 Chintan Patel. All rights reserved.
//

import UIKit

class SlashViewController: UIViewController {

    
    
    
    @IBOutlet weak var main_Logo: UIImageView!
    
    @IBOutlet weak var txt_Logo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        main_Logo.isHidden = true
        txt_Logo.isHidden = true
        perform(#selector(startAnimLogo), with: nil, afterDelay: 1.0)
        perform(#selector(startAnimText), with: nil, afterDelay: 1.5)
        perform(#selector(GOTONEXT), with: nil, afterDelay: 2.0)

        // Do any additional setup after loading the view.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func startAnimLogo() {
        
        main_Logo.isHidden = false

        main_Logo.slideIn(from: kFTAnimationBottom, in: main_Logo.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
        
    }
    
    @objc func startAnimText() {
        txt_Logo.isHidden = false

        txt_Logo.slideIn(from: kFTAnimationBottom, in: txt_Logo.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
        
    }
    
    @objc func GOTONEXT() {
       
        let viewcon = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(viewcon, animated: false)
        
    }
    
    @objc func temp(){
        
    }
    
    
}
